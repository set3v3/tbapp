// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // api_base_url: 'https://tbstarr.org'
  //api_base_url: 'http://111.93.177.62:8000',
  //img_base_url: 'http://111.93.177.62:8000',
  // api_base_url: 'https://tbstarr.org/api',
  // img_base_url: 'https://tbstarr.org/API',
  api_base_url: 'http://staging.tbstarr.org/api',
  img_base_url: 'http://staging.tbstarr.org/API',


  // api_base_url: 'http://10.0.8.222:8000'
  // api_base_url: 'http://35.181.71.135:8000'
  // api_base_url: 'http://35.181.71.135:8000'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
