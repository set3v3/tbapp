
export declare type ToastType = 'success' | 'error' | 'warning';

export interface ToastExtraData {
  type: ToastType;
  message: string;
}
