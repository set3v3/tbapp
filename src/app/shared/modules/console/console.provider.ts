import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class ConsoleProvider {
  public envVariable: boolean;
  constructor() {
    this.envVariable = environment.production;
  }
  log(info?, data?) {
    if (!this.envVariable) {
      return console.log(info, '===========', data);
    }
  }

}
