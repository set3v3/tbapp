import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class HelpSupportService {
  private SUPPORT_URL: string;
  private SUPPORT_LIST: string;

  constructor(
    private httpClient: HttpClient
  ) {
    this.SUPPORT_URL = '/admin/supportForm';
    this.SUPPORT_LIST = '/admin/supportFormList';

  }
  Submit(info): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.SUPPORT_URL
    };
    return this.httpClient.post<any>(request.url, info);
  }
  list(info): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.SUPPORT_LIST
    };
    return this.httpClient.post<any>(request.url, info);
  }
}
