import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class FacilityMasterService {
  private FACLITY_LIST: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.FACLITY_LIST = '';
  }
  getFacilityList(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.FACLITY_LIST
    };
    return this.httpClient.post<any>(request.url, param);
  }
}
