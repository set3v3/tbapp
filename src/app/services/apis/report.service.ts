import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PatientListApiResponce, IndependentPatientReportApiResponce, TbConfirmedReportReportApiResponce, PatientListExportReportApiResponce, ExportReportApiResponce } from 'src/app/models/report';


@Injectable()
export class ReportService {
  private REPORT_LIST: string;
  private SCREENED_REPORT_EXEL_EXPORT: string;
  private REPORT_EXEL_EXPORT: string;
  private ALL_REPORT_LIST: string;
  private TB_CONFRIM_REPORT: string;
  private INCENTIVE_REPORT: string;
  private INCENTIVE_DETAILS: string;

  constructor(
    private httpClient: HttpClient
  ) {
    this.REPORT_LIST = '/admin/patientList';
    this.SCREENED_REPORT_EXEL_EXPORT = '/admin/patientListExport';
    this.ALL_REPORT_LIST = '/admin/independentPatientReport';
    this.REPORT_EXEL_EXPORT = '/admin/PatientReportExport';
    this.TB_CONFRIM_REPORT = '/admin/tbConfirmedReport';
    this.INCENTIVE_REPORT = '/admin/referralIncentiveReport';
    this.INCENTIVE_DETAILS = '/admin/referralPatientsList';
  }
  getPatienReportList(param): Observable<PatientListApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.REPORT_LIST
    };
    return this.httpClient.post<PatientListApiResponce>(request.url, param);
  }
  getincentiveReportList(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.INCENTIVE_REPORT
    };
    return this.httpClient.post<any>(request.url, param);
  }
  getReportList(param): Observable<IndependentPatientReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.ALL_REPORT_LIST
    };
    return this.httpClient.post<IndependentPatientReportApiResponce>(request.url, param);
  }
  getTbconfromationReportList(param): Observable<TbConfirmedReportReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.TB_CONFRIM_REPORT
    };
    return this.httpClient.post<TbConfirmedReportReportApiResponce>(request.url, param);
  }
  incentiveDetais(param): Observable<any> {
    const request = {
      url: AppConst.API_BASE_URL + this.INCENTIVE_DETAILS
    };
    return this.httpClient.post<any>(request.url, param);
  }
  downloadScreenedReportExport(data): Observable<PatientListExportReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.SCREENED_REPORT_EXEL_EXPORT
    };
    const params = new HttpParams()
      .set('hasTb', data.hasTb)
      .set('isHiv', data.isHiv)
      .set('startDate', data.startDate)
      .set('endDate', data.endDate)
      .set('searchText', data.searchText);
    return this.httpClient.get<PatientListExportReportApiResponce>(request.url, { params });
  }
  downloadReportExport(param): Observable<ExportReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.REPORT_EXEL_EXPORT
    };
    return this.httpClient.post<ExportReportApiResponce>(request.url, param);
  }
}
