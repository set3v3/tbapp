import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import {
  DashbordApiResponce, LgaWiseReportApiResponce,
  StateWiseReportApiResponce, AdminDashboardGraphApiResponce
} from 'src/app/models/dashbord';


@Injectable()
export class DashBordService {
  private REPORTTAB: string;
  private REPORTGRAPH: string;
  private STATEWISEREPORT: string;
  private LGAWISEREPORT_TABLE: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.REPORTTAB = '/admin/adminDashboardData';
    this.REPORTGRAPH = '/admin/adminDashboardPatientGraph';
    this.STATEWISEREPORT = '/admin/stateWiseReport';
    this.LGAWISEREPORT_TABLE = '/admin/lgaWiseReport';
  }
  getreportTab(param): Observable<DashbordApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.REPORTTAB
    };
    return this.httpClient.post<DashbordApiResponce>(request.url, param);
  }

  getGraphData(param): Observable<AdminDashboardGraphApiResponce> {
    // console.log(param);
    const request = {
      url: AppConst.API_BASE_URL + this.REPORTGRAPH
    };
    return this.httpClient.post<AdminDashboardGraphApiResponce>(request.url, param);
  }
  getstateWiseReportData(param): Observable<StateWiseReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.STATEWISEREPORT
    };
    return this.httpClient.post<StateWiseReportApiResponce>(request.url, param);
  }
  getLGAWiseInformationTable(param): Observable<LgaWiseReportApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.LGAWISEREPORT_TABLE
    };
    return this.httpClient.post<LgaWiseReportApiResponce>(request.url, param);
  }
}
