import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  UserlistApiResponse, UsereditPermissionApiResponse,
  ADMINUserlistApiResponse, AddAdminUserApiResponse,
  EditAdminUserApiResponse, ImeiApiResponse, AdminUserReportExportApiResponse, AppUserReportExportApiResponse, AddressDetailsApiResponse
} from 'src/app/models/usermaster';


@Injectable()
export class UserMasterService {
  private USER_LIST: string;
  private USER_STATUS_UPDATE: string;
  private ADD_ADMIN_USER: string;
  private ADMIN_USER_LIST: string;
  private EDIT_ADMIN_USER: string;
  private SHOW_IMEI_NO: string;
  private ADMIN_USER_REPORT_EXEL_EXPORT: string;
  private APP_USER_REPORT_EXEL_EXPORT: string;
  private STATE_LGA_RELETIONSHIP: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.USER_LIST = '/admin/userList';
    this.USER_STATUS_UPDATE = '/admin/editPermission';
    this.ADD_ADMIN_USER = '/admin/addUser';
    this.ADMIN_USER_LIST = '/admin/adminUserList';
    this.EDIT_ADMIN_USER = '/admin/editUser';
    this.SHOW_IMEI_NO = '/admin/imeiVerification';
    this.ADMIN_USER_REPORT_EXEL_EXPORT = '/admin/adminUserListExport';
    this.APP_USER_REPORT_EXEL_EXPORT = '/admin/userListExport';
    this.STATE_LGA_RELETIONSHIP = '/admin/deatilsForindependentUser';
  }
  getUserList(param): Observable<UserlistApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.USER_LIST
    };
    return this.httpClient.post<UserlistApiResponse>(request.url, param);
  }
  UserUpdate(userinfo): Observable<UsereditPermissionApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.USER_STATUS_UPDATE
    };
    return this.httpClient.post<UsereditPermissionApiResponse>(request.url, userinfo);

  }
  AddAdminUser(param): Observable<AddAdminUserApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_ADMIN_USER
    };
    return this.httpClient.post<AddAdminUserApiResponse>(request.url, param);
  }
  getAdminUserList(param): Observable<ADMINUserlistApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADMIN_USER_LIST
    };
    return this.httpClient.post<ADMINUserlistApiResponse>(request.url, param);
  }
  AdminUserUpdate(userinfo): Observable<EditAdminUserApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_ADMIN_USER
    };
    return this.httpClient.post<EditAdminUserApiResponse>(request.url, userinfo);
  }
  ShowImeiNo(info): Observable<ImeiApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.SHOW_IMEI_NO
    };
    return this.httpClient.post<ImeiApiResponse>(request.url, info);
  }
  downloadAdminUserExport(info): Observable<AdminUserReportExportApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADMIN_USER_REPORT_EXEL_EXPORT
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<AdminUserReportExportApiResponse>(request.url, { params });
  }
  downloadAppUserReportExport(info): Observable<AppUserReportExportApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.APP_USER_REPORT_EXEL_EXPORT
    };
    const params = new HttpParams()
      .set('search', info.search);
    return this.httpClient.get<AppUserReportExportApiResponse>(request.url, { params });
  }
  getlgaState(): Observable<AddressDetailsApiResponse> {
    const request = {
      url: AppConst.API_BASE_URL + this.STATE_LGA_RELETIONSHIP
    };
    return this.httpClient.get<AddressDetailsApiResponse>(request.url);
  }
}
