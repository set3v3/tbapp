import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { LoginApiResponce, ResetPasswordApiResponce, LogOutApiResponce, ForGotPasswordApiResponce } from 'src/app/models/auth';


@Injectable()
export class AuthService {
  private LOGIN_URL: string;
  private CHNAGE_PASSWORD: string;
  private LOGOUT_URL: string;
  private FORGOT_PASS_URL: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.LOGIN_URL = '/admin/logIn';
    this.CHNAGE_PASSWORD = '/admin/changePassword';
    this.LOGOUT_URL = '/admin/logOut';
    this.FORGOT_PASS_URL = '/admin/forgetPassword';

  }
  dologin(credentials): Observable<LoginApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.LOGIN_URL
    };
    return this.httpClient.post<LoginApiResponce>(request.url, credentials);
  }
  resetPassword(credentials): Observable<ResetPasswordApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.CHNAGE_PASSWORD
    };
    return this.httpClient.post<ResetPasswordApiResponce>(request.url, credentials);
  }
  logout(): Observable<LogOutApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.LOGOUT_URL
    };
    return this.httpClient.get<LogOutApiResponce>(request.url);
  }
  forgotPassword(credentials): Observable<ForGotPasswordApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.FORGOT_PASS_URL
    };
    return this.httpClient.post<ForGotPasswordApiResponce>(request.url, credentials);
  }

}
