import { Injectable } from '@angular/core';
import { AppConst } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { RoleListingApiResponce, EDITRoleApiResponce, ADDRoleApiResponce } from 'src/app/models/role';


@Injectable()
export class RoleMasterService {
  private ADD_ROLE: string;
  private ROLE_LISTING: string;
  private EDIT_ROLE: string;
  constructor(
    private httpClient: HttpClient
  ) {
    this.ADD_ROLE = '/admin/addRole';
    this.EDIT_ROLE = '/admin/editRolePermission';
    this.ROLE_LISTING = '/admin/getRole';
  }
  addrole(param): Observable<ADDRoleApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.ADD_ROLE
    };
    return this.httpClient.post<ADDRoleApiResponce>(request.url, param);
  }
  rolelisting(): Observable<RoleListingApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.ROLE_LISTING
    };
    return this.httpClient.get<RoleListingApiResponce>(request.url);
  }
  editrole(param): Observable<EDITRoleApiResponce> {
    const request = {
      url: AppConst.API_BASE_URL + this.EDIT_ROLE
    };
    return this.httpClient.put<EDITRoleApiResponce>(request.url, param);
  }
}
