import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router/src/router_state';
import { Router } from '@angular/router';
import { UserStorageProvider } from '../storage/user-storage.service';

@Injectable()
export class SessionGuard implements CanActivate {
  constructor(
    private router: Router,
    private userStorage: UserStorageProvider
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.userStorage.get()) {
      this.router.navigate(['post-auth/dashboard']);
    }
    return true;
  }
}
