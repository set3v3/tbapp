import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { UserStorageProvider } from '../storage/user-storage.service';
import { PermissionProvider } from '../permission/permission.service';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private myRoute: Router,
    private userStorage: UserStorageProvider,
    private Permission: PermissionProvider) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.userStorage.get()) {
      const urarry = state.url.split('/');
      if (urarry.length > 0) {
        const url = urarry[urarry.length - 1];
        // tslint:disable-next-line:triple-equals
        if (url === 'dashboard' || url === 'lga-wise-information' || url === 'no-access'
          || url === '404' || url === 'referral-incentive-report' || url === 'referral-patients-details') {
          return true;
        } else if (this.Permission.AuthGuardPermission(url)) {
          return true;
        }
        this.myRoute.navigate(['post-auth/no-access']);
      }
    } else {
      this.myRoute.navigate(['pre-auth/login'], {
        queryParams: { referer: state.url }
      });

    }
  }
}
