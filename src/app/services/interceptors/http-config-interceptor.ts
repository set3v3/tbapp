import { UserStorageProvider } from './../storage/user-storage.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { AppConst } from '../../app.constants';
import { ToastProvider } from '../../shared/modules/toast/toast.provider';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { CryptoProvider } from '../crypto/crypto.service';
import { Router } from '@angular/router';
import { AuthService } from '../apis/auth.service';
import { LogOutApiResponce } from 'src/app/models/auth';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  // userInfo: UserInfo;
  constructor(private toast: ToastProvider,
    // tslint:disable-next-line:align
    private userStorage: UserStorageProvider,
    // tslint:disable-next-line:align
    private fileEncryption: CryptoProvider,
    // tslint:disable-next-line:align
    private router: Router,
    // tslint:disable-next-line:align
    private authApi: AuthService

  ) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let customReq;
    const urarry = req.url.split('/');
    let url;
    if (urarry.length > 0) {
      url = urarry[urarry.length - 1];
      console.log(url);
    }
    if ((url !== 'supportForm') && (req.method.toLowerCase() === 'post' || req.method.toLowerCase() === 'put')) {
      customReq = req.clone({
        withCredentials: true,
        headers: new HttpHeaders({
          apikey: AppConst.API_KEY,
          token: this.userStorage.get() ? this.userStorage.get().data.accessToken : ''
        }),
        body: { EncryptionData: this.fileEncryption.encryptObj(req.body) }
      });
    } else {
      customReq = req.clone({
        withCredentials: true,
        headers: new HttpHeaders({
          apikey: AppConst.API_KEY,
          token: this.userStorage.get() ? this.userStorage.get().data.accessToken : ''
        })
      });
    }
    // const customReq = req.clone({
    //   withCredentials: true,
    //   headers: new HttpHeaders({
    //     apikey: AppConst.API_KEY,
    //     token: this.userStorage.get() ? this.userStorage.get().data.accessToken : ''
    //   })
    // });
    // console.log(customReq);
    return next.handle(customReq).do((event: HttpEvent<any>) => {
      // console.log(event);
      if (event instanceof HttpResponse) {
        const urarry = event.url.split('/');
        if (urarry.length > 0) {
          const url = urarry[urarry.length - 1];
          // tslint:disable-next-line:triple-equals
          if (event.body.hasOwnProperty('data') && url != 'en.json') {
            event.body.data = this.fileEncryption.decryptObj(event.body.data);  // Encryption
            return event;
          }
        }
      }
    }, (err: any) => {
      // console.log('err', err);
      if (err instanceof HttpErrorResponse) {
        // console.log(err);
        if (err.status === AppConst.HTTP_STATUS.BAD_REQUEST || err.status === AppConst.HTTP_STATUS.UN_AUTHORIZED) {
          this.toast.error(err.error.message);
          if (err.status === AppConst.HTTP_STATUS.UN_AUTHORIZED) {
            this.logOut();
          }
        } else if (err.status === AppConst.HTTP_STATUS.SERVER_INTERNAL_ERROR) {
          const error = err.error.Errors;
          // console.log('err status ', error);
          for (const k in error) {
            if (k) {
              this.toast.error(error[k][0].message);
            }
          }
        } else {
          // console.log('err status 2 ', err);
        }
      }
    });
  }
  logOut() {
    this.authApi.logout().subscribe((responseData: LogOutApiResponce) => {
      if (responseData.success) {
        this.router.navigate(['/']);
        this.userStorage.clear();
        this.toast.success(responseData.message);
      }
    }, error => {
    });
  }
}
