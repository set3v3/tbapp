import { Injectable } from '@angular/core';
import { UserStorageProvider } from '../storage/user-storage.service';

//   */
@Injectable()

export class PermissionProvider {
  permissionlist: any;
  constructor(
    private userStorage: UserStorageProvider,
  ) {

  }
  permission(facility, permitioNmae): any {
    this.permissionlist = this.userStorage.get().data.role.facility;
    const index = this.permissionlist.findIndex(x => x.name === facility);
    if (index > -1) {
      const index2 = this.permissionlist[index].permission.findIndex(x => x.permissionName === permitioNmae);
      // console.log(this.permissionlist[index].permission);
      if (index2 > -1) {
        return this.permissionlist[index].permission[index2].status;
      }
    }
    // return true;
  }
  sidePanelpermission(facility): any {
    this.permissionlist = this.userStorage.get().data.role.facility;
    const index = this.permissionlist.findIndex(x => x.name === facility);
    if (index > -1) {
      // tslint:disable-next-line:triple-equals
      return this.permissionlist[index].status == '1' ? true : false;
    } else {
      // return true; /// must be remove after all permition done
    }
    // return true;
  }
  sidePanelURL(facility): any {
    this.permissionlist = this.userStorage.get().data.role.facility;
    const index = this.permissionlist.findIndex(x => x.name === facility);
    if (index > -1) {
      // tslint:disable-next-line:triple-equals
      return this.permissionlist[index].url;
    }
  }
  AuthGuardPermission(param): any {
    this.permissionlist = this.userStorage.get().data.role.facility;
    const index = this.permissionlist.findIndex(x => x.url === param);
    if (index > -1) {
      // tslint:disable-next-line:triple-equals
      return this.permissionlist[index].status == '1' ? true : false;
    }
  }
}

