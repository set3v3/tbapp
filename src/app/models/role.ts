export interface RoleListingApiResponce {
  resCode: number;
  data: Array<RoleINFO>;
}
export interface RoleINFO {
  _id: string;
  roleName: string;
  facility: Array<FacilityINFO>;
}
export interface FacilityINFO {
  permission: Array<PermissionINFO>;
  _id: string;
  name: string;
  status: boolean;
}
export interface PermissionINFO {
  _id: string;
  permissionName: string;
  status: boolean;
}

export interface EDITRoleApiResponce {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
export interface ADDRoleApiResponce {
  resCode: number;
  success: boolean;
  message: string;
  result: string;
}
