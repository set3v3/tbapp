export interface PatientListApiResponce {
  resCode: number;
  success: boolean;
  page: number;
  totalPage: number;
  totalCount: number;
  result: string;
  message: string;
  data: Array<PatientListInfo>;
}
export interface PatientListInfo {
  firstName: string;
  visitDate: any;
  phoneNumber: string;
  secondaryPhoneNumber: string;
  age: string;
  sex: string;
  tbstatus: any;
  hivStatus: any;
  Patient_Record_Number: string;
  Had_TB_Before: any;
  address: string;
}
export interface IndependentPatientReportApiResponce {
  resCode: number;
  success: boolean;
  page: number;
  totalPage: number;
  totalCount: number;
  result: string;
  message: string;
  data: Array<IndependentPatientInfo>;
}
export interface IndependentPatientInfo {
  firstName: string;
  middleName: string;
  lastName: string;
  phoneNumber: string;
  age: string;
  sex: string;
  initiateTreatment: number;
  referred: number;
  confirmedPositive: number;
  PatientRecordNumber: string;
  address: string;
  date: any;
}
export interface TbConfirmedReportReportApiResponce {
  resCode: number;
  success: boolean;
  page: number;
  totalPage: number;
  totalCount: number;
  result: string;
  message: string;
  data: Array<TbConfirmedInfo>;
}
export interface TbConfirmedInfo {
  firstName: string;
  middleName: string;
  lastName: string;
  phoneNumber: string;
  age: string;
  sex: string;
  confirmedPositive: number;
  PatientRecordNumber: string;
  date: any;
}
export interface PatientListExportReportApiResponce {
  resCode: number;
  success: boolean;
  result: string;
  message: string;
  data: string;
}
export interface ExportReportApiResponce {
  resCode: number;
  success: boolean;
  result: string;
  message: string;
  data: string;
}
