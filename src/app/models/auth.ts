export interface LoginApiResponce {
  success: boolean;
  result: string;
  message: string;
  resCode: number;
  data: UserInfo;
}
export interface UserInfo {
  accessToken: string;
  designation: string;
  firstName: string;
  phoneNumber: string;
  surName: string;
  userType: string;
}
export interface ResetPasswordApiResponce {
  success: boolean;
  result: string;
  message: string;
  resCode: number;
}
export interface LogOutApiResponce {
  success: boolean;
  result: string;
  message: string;
  resCode: number;
}
export interface ForGotPasswordApiResponce {
  success: boolean;
  resCode: number;
  result: string;
  message: string;
  data: ForGotPasswordInFo;
}
export interface ForGotPasswordInFo {
  resettoken: number;
}

