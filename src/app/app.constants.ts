import { HttpStatus } from './models/http';
import { environment } from '../environments/environment';
/**
 * All application constant should define here.
 *
 * @export
 * AppConst
 * @author TATHAGATA SUR
 */
export class AppConst {
  public static readonly API_BASE_URL: string = environment.api_base_url;
  public static readonly IMG_BASE_URL: string = environment.img_base_url;

  /**  Data encryption secret key */
  // public static readonly ENC_KEY: string = '!InT@TbAdmin!#';
  public static readonly ENC_KEY: string = '1234564y56yw45tw45tyw4twqtg';
  /**  API key */
  // tslint:disable-next-line:max-line-length
  public static readonly API_KEY: string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0YkFwcEBpbnRAMTIzIiwibmFtZSI6InBhc3MiLCJpYXQiOjE1MTYyMzkwMjJ9.CwnApT_ogvtZRF9Mwkzox2ieAh61rw08Xdv7LgTKLHw';
  /** Enviromental variable */
  public static readonly ENV_VARIABLE: string = 'development';
  // public static readonly ENV_VARIABLE: string = 'prod';


  /**  server HTTP header status code */
  public static readonly HTTP_STATUS: HttpStatus = {
    OK: 200,
    BAD_REQUEST: 400,
    UN_AUTHORIZED: 401,
    NOT_FOUND: 404,
    SERVER_INTERNAL_ERROR: 500,
    NO_RECORD: 204
  };
  public static readonly MONTH: Array<any> = [
    { name: 'Last 7 Days', id: 1 },
    { name: 'Last 15 Days', id: 2 },
    { name: 'This Month', id: 3 },
    { name: 'Last Year', id: 4 },
    { name: 'Custom', id: 5 }
  ];
  public static readonly MOBILE_USER_TYPE: Array<any> = [
    { name: 'Ppmv', id: 1 },
    { name: 'Labtech', id: 2 },
    { name: 'Clinician', id: 3 },
  ];
  public static readonly DATA_POTAL_USER_TYPE: Array<any> = [
    { name: 'Generalist user', id: 1 },
    { name: 'Program Manager', id: 2 },
    { name: 'Super-admin', id: 3 },
  ];
  public static readonly TB: Array<any> = [
    { name: 'Non-Presumptive', id: 0 },
    { name: 'Presumptive', id: 1 }
  ];
  public static readonly HIV: Array<any> = [
    { name: 'Negative', id: 0 },
    { name: 'Positive', id: 1 },
    { name: 'Unknown', id: 2 }
  ];
  public static readonly ReferralStatus: Array<any> = [
    { name: 'Referred', id: 1 },
    { name: 'Not Referred', id: 0 },
  ];
  public static readonly TreatmentInitiated: Array<any> = [
    { name: 'Yes', id: 1 },
    { name: 'No', id: 0 },
  ];
  public static readonly TbStatus: Array<any> = [
    { name: 'Positive', id: 1 },
    { name: 'Negative', id: 0 },
  ];
  public static readonly RStatus: Array<any> = [
    { name: 'Referred', id: 1 },
    { name: 'Not Referred', id: 0 },
  ];
  public static readonly Issuesource: Array<any> = [
    { name: 'App', id: 'App' },
    // { name: 'Data Portal', id: 'Data Portal' },
  ];
  public static readonly IssueType: Array<any> = [
    { name: 'low', id: 'low' },
    { name: 'Medium', id: 'medium' },
    { name: 'Severe', id: 'severe' },
  ];
  public static readonly USER_TYPE: Array<any> = [
    { name: 'Generalist user', id: 'Generalist user' },
    { name: 'Program Manager', id: 'Program Manager' },
    { name: 'Super-admin', id: 'Super-admin' }
  ];
  public static readonly APP_USER_TYPE: Array<any> = [
    { name: 'Clinician', id: 'clinician' },
    { name: 'Independent', id: 'independent' },
    { name: 'Lab', id: 'lab' },
    { name: 'PPMV', id: 'ppmv' }
  ];
  public static readonly LANGUAGE: Array<any> = [
    { name: 'English', id: 'English' },
    // { name: 'Housa', id: 'Housa' }
  ];
  public static readonly MODE: Array<any> = [
    // { name: 'Offline', id: 'offline' },
    { name: 'Online', id: 'online' }
  ];
  public static readonly MONTH_LIST: Array<any> = [
    { name: 'January', id: '01' },
    { name: 'February', id: '02' },
    { name: 'March', id: '03' },
    { name: 'April', id: '04' },
    { name: 'May', id: '05' },
    { name: 'June', id: '06' },
    { name: 'July', id: '07' },
    { name: 'August', id: '08' },
    { name: 'September', id: '09' },
    { name: 'October', id: '10' },
    { name: 'November', id: '11' },
    { name: 'December', id: '12' },
  ];
  public static readonly YEAR_LIST: Array<any> = [
    '2018'
  ];
  public static readonly USER_MANAGEMET: Array<any> = [{
    name: 'user',
    status: '1',
    permission: [
      { permissionName: 'create', status: true },
      { permissionName: 'view', status: true },
      { permissionName: 'edit', status: true }
    ]
  },
  {
    name: 'role',
    status: '1',
    permission: [
      { permissionName: 'create', status: true },
      { permissionName: 'view', status: true },
      { permissionName: 'edit', status: true }
    ]
  },
  {
    name: 'admin-user',
    status: '1',
    permission: [
      { permissionName: 'create', status: true },
      { permissionName: 'view', status: true },
      { permissionName: 'edit', status: true }
    ]
  },
  {
    name: 'screened-report',
    status: '1',
    permission: [
      { permissionName: 'create', status: true },
      { permissionName: 'view', status: true },
      { permissionName: 'edit', status: true }
    ]
  }
  ];
  public static readonly PPMVMENU: Array<any> = [
    { name: 'Home', id: 'Home ' },
    { name: 'Screen New Client', id: 'Screen New Client' },
    { name: 'My Clients', id: 'My Clients' },
    { name: 'Dashboard', id: 'Dashboard' },
    { name: 'Edit Profile', id: 'Edit Profile' },
    { name: 'Change Password', id: 'Change Password' },
    { name: 'Help & Support', id: 'Help & Support' },
    { name: 'Logout', id: 'Logout' }
  ];
  public static readonly IP_MENU: Array<any> = [
    { name: 'Home', id: 'Home ' },
    { name: 'Dashboard', id: 'Dashboard' },
    { name: 'Edit Profile', id: 'Edit Profile' },
    { name: 'Change Password', id: 'Change Password' },
    { name: 'Help & Support', id: 'Help & Support' },
    { name: 'Logout', id: 'Logout' }
  ];
  public static readonly CLINIC_MENU: Array<any> = [
    { name: 'Home', id: 'Home ' },
    { name: 'Screen New Client', id: 'Screen New Client' },
    { name: 'Testing / Diagnosis', id: 'Testing / Diagnosis' },
    { name: 'My Clients', id: 'My Clients' },
    { name: 'Dashboard', id: 'Dashboard' },
    { name: 'Edit Profile', id: 'Edit Profile' },
    { name: 'Change Password', id: 'Change Password' },
    { name: 'Help & Support', id: 'Help & Support' },
    { name: 'Logout', id: 'Logout' }
  ];
  public static readonly LAB_MENU: Array<any> = [
    { name: 'Home', id: 'Home ' },
    { name: 'Screen New Client', id: 'Screen New Client' },
    { name: 'Testing / Diagnosis', id: 'Testing / Diagnosis' },
    { name: 'My Clients', id: 'My Clients' },
    { name: 'Dashboard', id: 'Dashboard' },
    { name: 'Edit Profile', id: 'Edit Profile' },
    { name: 'Change Password', id: 'Change Password' },
    { name: 'Help & Support', id: 'Help & Support' },
    { name: 'Logout', id: 'Logout' }
  ];
  public static readonly PDFURL: Array<any> = [
    { name: 'Independent', filename: 'TB%20STARR_IndependentProviders%20Manual_V1.pdf' },
    { name: 'PPMV', filename: 'TB_STARR_PPMV_V1.pdf' },
    { name: 'Clinic', filename: 'TB_STARR_Clinic_V1.pdf' },
    { name: 'Lab', filename: 'TB_STARR_Lab_V1.pdf' }
  ];

}
