import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppSideNav } from '../../models/ui/app-side-nav';
import { AccordionSideNav } from '../../models/ui/app-side-nav';
import { AuthService } from '../../services/apis/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CryptoProvider } from '../../services/crypto/crypto.service';
import { UserStorageProvider } from '../../services/storage/user-storage.service';
import { AppConst } from '../../app.constants';
import { ChangepasswordComponent } from './change-password/change-password.component';
import { PermissionProvider } from 'src/app/services/permission/permission.service';

@Component({
  selector: 'app-post-auth',
  templateUrl: './post-auth.component.html',
  styleUrls: ['./post-auth.component.scss']
})
export class PostAuthComponent implements OnInit {

  // sideMenuList: Array<AppSideNav>;
  sideMenuList: Array<any>;
  AccordionMenuList: Array<AccordionSideNav>;
  userInfo: any;
  usertype: any;
  constructor(
    private translate: TranslateService,
    private authSrv: AuthService,
    private router: Router,
    private crypto: CryptoProvider,
    private userStorage: UserStorageProvider,
    private dialog: MatDialog,
    public Permission: PermissionProvider,
  ) {
    this.userInfo = this.userStorage.get();
    this.usertype = this.userStorage.get().data.role.roleName;
    console.log(this.usertype);
    this.sideMenuList = [
      // tslint:disable-next-line:max-line-length
      { name: 'SIDE_NAV.TXT_DASHBOARD', matIcon: 'dashboard.png', canAccess: true, routeUrl: 'dashboard', childmenu: [] },
      {
        // tslint:disable-next-line:max-line-length
        name: 'SIDE_NAV.TXT_USER', matIcon: 'user.png', routeUrl: '', canAccess: (this.Permission.sidePanelpermission('Mobile app user') || this.Permission.sidePanelpermission('Data portal user')), childmenu: [
          {
            name: 'SIDE_NAV.APP_USER', matIcon: 'app-user.png',
            routeUrl: 'user', canAccess: this.Permission.sidePanelpermission('Mobile app user')
          },
          {
            name: 'SIDE_NAV.TXT_USER_TYPE', matIcon: 'web-user.png',
            routeUrl: 'admin-user', canAccess: this.Permission.sidePanelpermission('Data portal user')
          }
        ]
      },
      // tslint:disable-next-line:max-line-length
      { name: 'SIDE_NAV.TXT_ROLE', matIcon: 'role-management.png', routeUrl: 'user-role', childmenu: [], canAccess: this.Permission.sidePanelpermission('role') },
      {
        // tslint:disable-next-line:max-line-length
        name: 'SIDE_NAV.TXT_REPORT', matIcon: 'report-icon.png', routeUrl: '', canAccess: (this.Permission.sidePanelpermission('screened report') || this.Permission.sidePanelpermission('referred report') || this.Permission.sidePanelpermission('TB confirmation report') || this.Permission.sidePanelpermission('treatment report') || this.Permission.sidePanelpermission('presumptive referral status')), childmenu: [
          {
            name: 'SIDE_NAV.TXT_SCREENED_REPORT', matIcon: 'screened-report-icon.png',
            routeUrl: 'screened-report', canAccess: this.Permission.sidePanelpermission('screened report')
          },
          {
            name: 'SIDE_NAV.TXT_REFFERED_REPORT', matIcon: 'reffered-report-icon.png',
            routeUrl: 'referred-report', canAccess: this.Permission.sidePanelpermission('referred report')
          },
          {
            name: 'SIDE_NAV.TB_CONFRIM', matIcon: 'tb-report-icon.png',
            routeUrl: 'tb-confirmation-report', canAccess: this.Permission.sidePanelpermission('TB confirmation report')
          },
          {
            name: 'SIDE_NAV.TREATMENT_REPORT', matIcon: 'treatment-report-icon.png',
            routeUrl: 'treatment-report', canAccess: this.Permission.sidePanelpermission('treatment report')
          },
          {
            name: 'SIDE_NAV.P_NOT_REFER_REPORT', matIcon: 'presumptive-report-icon.png',
            routeUrl: 'presumptive-not-referred-report', canAccess: this.Permission.sidePanelpermission('presumptive referral status')
          },
          // {
          //   name: 'SIDE_NAV.REFERRAL_INCENTIVE_REPORT', matIcon: 'presumptive-report-icon.png',
          //   routeUrl: 'referral-incentive-report', canAccess: true
          // }
        ]
      },
      {
        name: 'SIDE_NAV.TXT_HELP_SUPPORT', matIcon: 'role-management.png', routeUrl: 'help-support', childmenu: [],

        canAccess: this.Permission.sidePanelpermission('Help & Support')
      },
      // { name: 'SIDE_NAV.TXT_FACILITY', matIcon: 'feedback', routeUrl: 'facility', childmenu: [] },
      // { name: 'SIDE_NAV.TXT_USER_CATEGORY', matIcon: 'category', routeUrl: 'user-category', childmenu: [] },

      // { name: 'SIDE_NAV.TXT_FACILITY', matIcon: 'feedback', routeUrl: 'facility', childmenu: [] },
      // { name: 'SIDE_NAV.TXT_CLIENTS', matIcon: 'accessibility', routeUrl: 'client', childmenu: [] }
    ];
  }
  getImgUrl(imagename) {
    return 'assets/images/icon/' + imagename;
  }
  ngOnInit() { }

  isLargeScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width > 1024) {
      return true;
    } else {
      return false;
    }
  }
  // checkAccess(menuName) {

  //   return true;
  // }
  /*
    * function use to logOut
    *
    * @memberof PostAuthComponent
    */
  logOut() {
    this.userStorage.clear();
    this.router.navigate(['/']);
  }
  goToLink(link: string) {
    const url = this.Permission.sidePanelURL(link);
    window.open(url, 'blank');
  }
}
