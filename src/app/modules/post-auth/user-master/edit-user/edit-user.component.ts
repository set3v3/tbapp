import { AppConst } from '../../../../app.constants';
import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { UsereditPermissionApiResponse } from 'src/app/models/usermaster';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
  providers: []
})
export class EditUserComponent implements OnInit {
  EditUserform: FormGroup;
  showLoader: boolean;
  @Output() UserOnEdit = new EventEmitter<UsereditPermissionApiResponse>(true);
  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<EditUserComponent>,
    private consoleProvider: ConsoleProvider,
    private UserMasterApi: UserMasterService,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.consoleProvider.log('EditUser', this.data);
  }
  ngOnInit() {
    this.FormBuild();

  }
  FormBuild() {
    this.EditUserform = new FormGroup({
      activeStatus: new FormControl(this.data.activeStatus, [
        Validators.required,
      ]),
      id: new FormControl(this.data.user_id, [
      ]),

    });
  }

  EditUser(userInfo) {
    if (this.EditUserform.valid) {
      this.showLoader = true;
      this.UserMasterApi.UserUpdate(userInfo).subscribe((response: UsereditPermissionApiResponse) => {
        this.showLoader = false;
        if (response.success) {
          this.UserOnEdit.emit(response);
          this.dialogRef.close();
          this.toast.success('User Updated Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }
  }
}
