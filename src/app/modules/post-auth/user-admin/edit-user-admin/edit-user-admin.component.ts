import { AppConst } from '../../../../app.constants';
import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { equalvalidator } from 'src/app/directives/validators/equal-validator';


@Component({
  selector: 'app-edit-user-admin',
  templateUrl: './edit-user-admin.component.html',
  styleUrls: ['./edit-user-admin.component.scss'],
  providers: []
})
export class EditUserAdminComponent implements OnInit {
  editAdminUserform: FormGroup;
  showLoader: boolean;
  RoleListing: Array<any>;
  StateList: Array<any>;
  LgaListing: Array<any>;
  statevalidation: boolean;
  @Output() OnEditAdmin = new EventEmitter<any>(true);
  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<EditUserAdminComponent>,
    private consoleProvider: ConsoleProvider,
    private RoleApi: RoleMasterService,
    private UserMasterApi: UserMasterService,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    // console.log(JSON.stringify(this.data));
    this.RoleListing = [];
    this.StateList = [];
    this.statevalidation = false;
    this.LgaListing = [];
    this.fetchRoleListing();
    this.getLgaState().then(() => {
      this.SetLga(this.data.state);
    });
  }
  /*
  * function build form with exting data
  * @param
  * @memberof EditUserAdminComponent
  */
  ngOnInit() {
    console.log(this.data);
    /* tslint:disable */
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /* tslint:enable */
    this.editAdminUserform = new FormGroup({
      id: new FormControl(this.data.user_id, [
      ]),
      roleId: new FormControl(this.data.hasOwnProperty('roleId') ? this.data.roleId ? this.data.roleId._id : '' : '', [
        Validators.required,
      ]),
      // email: new FormControl(this.data.hasOwnProperty('email') ? this.data.email : '', [
      // ]),
      firstName: new FormControl(this.data.hasOwnProperty('firstName') ? this.data.firstName : '', [
        Validators.required,
      ]),
      surName: new FormControl(this.data.hasOwnProperty('surName') ? this.data.surName : '', [
        Validators.required,
      ]),
      phoneNumber: new FormControl(this.data.hasOwnProperty('phoneNumber') ? this.data.phoneNumber : '', [
        Validators.required
      ]),
      state: new FormControl(this.data.hasOwnProperty('state') ? this.data.state : '', [
      ]),
      lga: new FormControl(this.data.hasOwnProperty('lga') ? this.data.lga : '', [
      ]),
      password: new FormControl('', [
      ]),
      confirmPassword: new FormControl('', [equalvalidator('password')
      ]),
      activeStatus: new FormControl(this.data.activeStatus, [
        Validators.required,
      ])
    });
    this.StateValidation(this.data.roleId._id);

    //this.statevalidation = true;

  }
  getLgaState() {
    return new Promise((resolve, reject) => {
      this.UserMasterApi.getlgaState().subscribe((res: any) => {
        console.log(res);
        if (res.success) {
          this.StateList = res.data.addressDetails;
          console.log(this.StateList);
          resolve();
        }

      }, (error) => {

        console.log(error);
        reject();
      });
    });
  }
  StateChange(event) {
    const index = this.StateList.findIndex(x => x.State === event.value);
    if (index > -1) {
      this.LgaListing = this.StateList[index].LGA;
    }
    console.log(this.LgaListing);
  }
  SetLga(value) {
    const index = this.StateList.findIndex(x => x.State === value);
    if (index > -1) {
      this.LgaListing = this.StateList[index].LGA;
    }
    console.log(this.LgaListing);
  }
  StateValidation(value) {
    if (value === '5da444785b04a31c1726802f') {
      this.editAdminUserform.get('state').setValidators([Validators.required]);
      this.editAdminUserform.get('lga').setValidators([Validators.required]);
      this.statevalidation = true;
    } else {
      this.editAdminUserform.get('state').setValidators([]);
      this.editAdminUserform.get('lga').setValidators([]);
      this.statevalidation = false;
    }
    this.editAdminUserform.get('state').updateValueAndValidity();
    this.editAdminUserform.get('lga').updateValueAndValidity();
  }
  /*
  * function use ferch  role for admin user
  * @param
  * @memberof EditUserAdminComponent
  */
  fetchRoleListing() {
    this.showLoader = true;
    this.RoleApi.rolelisting().subscribe((response: any) => {
      // console.log(response);
      this.showLoader = false;
      if (response.resCode === 200) {
        this.RoleListing = response.data;
      }
    }, (errorData: any) => {
      this.showLoader = false;
    });
  }
  /*
  * function use to api call  edit admin user
  * @param
  * @memberof EditUserAdminComponent
  */
  editUsertype(userInfo) {
    console.log(userInfo);
    // console.log(this.editAdminUserform.valid);
    if (this.editAdminUserform.valid) {
      this.showLoader = true;
      this.UserMasterApi.AdminUserUpdate(userInfo).subscribe((response: any) => {
        this.showLoader = false;
        // console.log(response);
        if (response.success) {
          this.OnEditAdmin.emit(response);
          this.dialogRef.close();
          this.toast.success('User Updated Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }
  }
}
