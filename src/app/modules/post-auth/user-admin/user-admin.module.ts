import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { UserAdminRouteModule } from './user-admin.routes.module';
import { UserAdminComponent } from './user-admin.component';
import { EditUserAdminComponent } from './edit-user-admin/edit-user-admin.component';
import { AddUserAdminComponent } from './add-user-admin/add-user-admin.component';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { ViewAdminUserComponent } from './view-user-admin/view-user-admin.component';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { MatSortModule } from '@angular/material';
@NgModule({
  imports: [
    UserAdminRouteModule,
    SharedModule,
    EcoFabSpeedDialModule,
    MatSortModule
  ],
  declarations: [
    UserAdminComponent,
    AddUserAdminComponent,
    EditUserAdminComponent,
    ViewAdminUserComponent
  ],
  entryComponents: [
    UserAdminComponent,
    AddUserAdminComponent,
    EditUserAdminComponent,
    ViewAdminUserComponent
  ],

  providers: [UserMasterService, RoleMasterService]
})
export class UserAdminModule { }
