import { AppConst } from '../../../../app.constants';
import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSelectChange } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { equalvalidator } from 'src/app/directives/validators/equal-validator';
import { UserMasterService } from 'src/app/services/apis/usermaster';


@Component({
  selector: 'app-add-user-admin',
  templateUrl: './add-user-admin.component.html',
  styleUrls: ['./add-user-admin.component.scss'],
  providers: []
})
export class AddUserAdminComponent implements OnInit {
  addAdminUserform: FormGroup;
  showLoader: boolean;
  RoleListing: Array<any>;
  StateList: Array<any>;
  LgaListing: Array<any>;
  statevalidation: boolean;
  @Output() OnAddAdmin = new EventEmitter<any>(true);

  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<AddUserAdminComponent>,
    private consoleProvider: ConsoleProvider,
    private RoleApi: RoleMasterService,
    private UserMasterApi: UserMasterService,

    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.RoleListing = [];
    this.StateList = [];
    this.statevalidation = false;
    this.LgaListing = [];
    this.fetchRoleListing();
    this.getLgaState();
  }
  /*
    * function  used for buildform
    * @param
    * @memberof AddUserAdminComponent
    */
  ngOnInit() {
    /* tslint:disable */
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /* tslint:enable */
    this.addAdminUserform = new FormGroup({
      roleId: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
        , Validators.pattern(emailRegx)
      ]),
      firstName: new FormControl('', [
        Validators.required,
      ]),
      surName: new FormControl('', [
        Validators.required,
      ]),
      phoneNumber: new FormControl('', [
        Validators.required,
      ]),
      state: new FormControl('', [
      ]),
      lga: new FormControl('', [
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      confirmPassword: new FormControl('', [
        Validators.required, equalvalidator('password')
      ]),
      activeStatus: new FormControl('1', [
        Validators.required,
      ]),

    });
  }
  getLgaState() {
    this.UserMasterApi.getlgaState().subscribe((res: any) => {
      console.log(res);
      if (res.success) {
        this.StateList = res.data.addressDetails;
        console.log(this.StateList);
      }

      // this.LgaListing = AppConst.LGA_NAME;
    }, (err) => {
    });
  }
  StateValidation(val) {
    console.log(val);
    if (val.value === '5da444785b04a31c1726802f') {
      this.addAdminUserform.get('state').setValidators([Validators.required]);
      this.addAdminUserform.get('lga').setValidators([Validators.required]);
      this.statevalidation = true;
    } else {
      this.addAdminUserform.get('state').setValidators([]);
      this.addAdminUserform.get('lga').setValidators([]);
      this.statevalidation = false;
    }
    this.addAdminUserform.get('state').updateValueAndValidity();
    this.addAdminUserform.get('lga').updateValueAndValidity();
  }
  StateChange(event) {
    const index = this.StateList.findIndex(x => x.State === event.value);
    if (index > -1) {
      this.LgaListing = this.StateList[index].LGA;
    }
    console.log(this.LgaListing);
  }
  /*
  * function  used for call api for fetching role
  * @param
  * @memberof AddUserAdminComponent
  */
  fetchRoleListing() {
    this.showLoader = true;
    this.RoleApi.rolelisting().subscribe((response: any) => {
      // console.log(response);
      this.showLoader = false;
      if (response.resCode === 200) {
        this.RoleListing = response.data;
      }
    }, (errorData: any) => {
      this.showLoader = false;
    });
  }
  /*
  * function  used for call api for fetching role
  * @param
  * @memberof AddUserAdminComponent
  */
  addAdminUser(userInfo) {
    // console.log(userInfo);
    if (this.addAdminUserform.valid) {
      this.showLoader = true;
      this.UserMasterApi.AddAdminUser(userInfo).subscribe((response: any) => {
        this.showLoader = false;
        // console.log(response);
        if (response.success) {
          this.OnAddAdmin.emit(response);
          this.dialogRef.close();
          this.toast.success('User Added Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }

  }
}
