import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '../../../services/apis/auth.service';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { equalvalidator } from '../../../directives/validators/equal-validator';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { ResetPasswordApiResponce } from 'src/app/models/auth';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.html',
  styleUrls: ['./change-password.scss']
})
export class ChangepasswordComponent implements OnInit {
  changePasswordform: FormGroup;
  showLoader: boolean;
  @Output() mealOnadd = new EventEmitter<any>(true);
  constructor(
    private authApi: AuthService,
    private dialogRef: MatDialogRef<ChangepasswordComponent>,
    private toast: ToastProvider,
    private consoleProvider: ConsoleProvider,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.showLoader = false;
  }

  ngOnInit() {
    this.changePasswordform = new FormGroup({
      oldPassword: new FormControl('', [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      confirmPassword: new FormControl('', [
        Validators.required, equalvalidator('password')
      ])
    });
  }
  /*
  * function use api hit for changePassword
  * @param
  * @memberof ChangepasswordComponent
  */
  changePassword(passwordinfo) {
    // console.log(passwordinfo);
    if (this.changePasswordform.valid) {
      this.showLoader = true;
      this.authApi.resetPassword(passwordinfo).subscribe((responseData: ResetPasswordApiResponce) => {
        this.showLoader = false;
        if (responseData.success) {
          this.dialogRef.close();
          this.toast.success(responseData.message);
        }
      }, error => {
        this.showLoader = false;
      });
    }

  }

}
