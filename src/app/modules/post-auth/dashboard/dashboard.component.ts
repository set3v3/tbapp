import { Component, OnInit, ViewChild } from '@angular/core';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { OwlCarousel } from 'ngx-owl-carousel';
import { MatTableDataSource } from '@angular/material';
import { AppConst } from 'src/app/app.constants';
import * as moment from 'moment';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import {
  DashbordApiResponce,
  LgaWiseReportApiResponce,
  StateWiseReportApiResponce, AdminDashboardGraphApiResponce
} from 'src/app/models/dashbord';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_nigeriaLow from "@amcharts/amcharts4-geodata/nigeriaLow";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DashBordService, ToastProvider, DatePipe],
})

export class DashboardComponent implements OnInit {
  public owlItems: any;
  public barchartObj: any;
  public doughnutChartObj: any;
  public dataSource: any;
  public datetitelbargraph: string;
  public datetiteldonorgraph: string;
  public dataSourcesuperadmin: any;
  isLoadingResults: boolean;
  public clickindex: number;
  public bartitle: any;
  public dateRange: any;
  public from: any;
  public to: any;
  public graphtag: any;
  public fromRegion: any;
  public toRegion: any;
  public Seletedlga: string;
  todaydate: any;
  duelColor: Array<any>;
  hoverduelColor: Array<any>;
  public selecteddateRange: any;
  public StaticColor: any;
  public displayedColumns = ['lga', 'Screened', 'Presumptive', 'Positive', 'Negative', 'Treatment'];
  public displayedColumnsuperadmin = ['lga', 'Screened', 'Presumptive', 'Positive', 'Negative', 'Treatment', 'active', 'lapsed'];
  public mapObj: any;
  public usertype: string;
  public showDatepicker: boolean;
  public showDatepickerRegion: boolean;
  public hoberStaticColor: string;
  public selecteddateRangeRegion: any;
  public LGAWiseInformation: any;
  public Regionwisedata: Array<any>;
  public datacount: number;
  public charrrt: any;

  @ViewChild('owlElement') owlElement: OwlCarousel;
  constructor(
    private consoleProvider: ConsoleProvider,
    private DashbordApi: DashBordService,
    private toast: ToastProvider,
    private datePipe: DatePipe,
    private router: Router,
    private userStorage: UserStorageProvider,
  ) {
    this.isLoadingResults = false;
    this.datetitelbargraph = '';
    this.datetiteldonorgraph = '';
    this.usertype = this.userStorage.get().data.role.roleName;
    this.dateRange = AppConst.MONTH;
    this.owlItems = [];
    this.showDatepicker = false;
    this.showDatepickerRegion = false;
    this.graphtag = '';
    this.bartitle = '';
    this.clickindex = 0;
    this.datacount = 0;
    this.todaydate = this.datePipe.transform(new Date().setDate(new Date().getDate()), 'yyyy-MM-dd').toString();
    this.from = '';
    this.to = '';
    this.Seletedlga = '';
    this.fromRegion = '';
    this.toRegion = '';
    this.selecteddateRange = 3;
    this.selecteddateRangeRegion = 3;
    // this.StaticColor = '#1a237e';
    this.hoberStaticColor = '#b41f2f';
    this.StaticColor = ['#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958',
      '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958', '#fe7958',
      '#fe7958', '#fe7958', '#fe7958', '#fe7958',
      '#fe7958', '#fe7958', '#fe7958', '#fe7958',
      '#fe7958', '#fe7958', '#fe7958', '#fe7958',
      '#fe7958', '#fe7958', '#fe7958', '#fe7958',
    ];
    this.duelColor = ['#1a237e', '#b41f2f', '#1a237e', '#b41f2f', '#1a237e', '#b41f2f', '#1a237e', '#b41f2f'];
    this.hoverduelColor = ['#b41f2f', '#1a237e', '#b41f2f', '#1a237e', '#b41f2f', '#1a237e', '#b41f2f', '#1a237e'];
    this.mapObj = {};
    this.barchartObj = {};
    this.doughnutChartObj = {};
    this.LGAWiseInformation = [];
    this.Regionwisedata = [];
    this.setBarChart();
    this.setdoughnutChart();



  }
  ngOnInit() {
    this.getDashbordaData().then(() => {
      this.datechangedBarChart(3);
    }, (err) => {

    });
    this.datechangeddoughnutChart(3);
    this.getLGAWiseInformationTable();
    this.choroplethmap();

  }
  choroplethmap() {
    const chart = am4core.create('chartdiv', am4maps.MapChart);

    // Set map definition
    chart.geodata = am4geodata_nigeriaLow;

    // Set projection
    // chart.projection = new am4maps.projections.AzimuthalEqualArea();

    // Create map polygon series
    const polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Set min/max fill color for each area
    polygonSeries.heatRules.push({
      property: 'fill',
      target: polygonSeries.mapPolygons.template,
      min: chart.colors.getIndex(1).brighten(1),
      max: chart.colors.getIndex(1).brighten(-0.3)
    });

    // Make map load polygon data (state shapes and names) from GeoJSON
    polygonSeries.useGeodata = true;

    // Set heatmap values for each state
    polygonSeries.data = [
      {
        id: 'NG-FC',
        value: 1
      },
      {
        id: 'NG-AB',
        value: 2
      },
      {
        id: 'NG-AD',
        value: 3
      },
      {
        id: 'NG-AK',
        value: 4
      },
      {
        id: 'NG-AN',
        value: 5
      },
      {
        id: 'NG-BA',
        value: 6
      },
      {
        id: 'NG-BY',
        value: 6
      },
      {
        id: 'NG-BE',
        value: 8
      },
      {
        id: 'NG-BO',
        value: 9
      },
      {
        id: 'NG-CR',
        value: 10
      },
      {
        id: 'NG-DE',
        value: 11
      },
      {
        id: 'NG-EB',
        value: 12
      },
      {
        id: 'NG-ED',
        value: 13
      },
      {
        id: 'NG-EK',
        value: 14
      },
      {
        id: 'NG-EN',
        value: 15
      },
      {
        id: 'NG-GO',
        value: 16
      },
      {
        id: 'NG-IM',
        value: 17
      },
      {
        id: 'NG-JI',
        value: 18
      },
      {
        id: 'NG-KD',
        value: 19
      },
      {
        id: 'NG-KN',
        value: 20
      },
      {
        id: 'NG-KT',
        value: 21
      }, {
        id: 'NG-KE',
        value: 22
      }, {
        id: 'NG-KO',
        value: 23
      },
      {
        id: 'NG-KW',
        value: 24
      },
      {
        id: 'NG-LA',
        value: 25
      },
      {
        id: 'NG-NA',
        value: 26
      }, {
        id: 'NG-NI',
        value: 27
      }, {
        id: 'NG-OG',
        value: 28
      },
      {
        id: 'NG-ON',
        value: 29
      },
      {
        id: 'NG-OS',
        value: 30
      },
      {
        id: 'NG-OY',
        value: 31
      }, {
        id: 'NG-PL',
        value: 32
      }, {
        id: 'NG-RI',
        value: 33
      },
      {
        id: 'NG-SO',
        value: 34
      },
      {
        id: 'NG-TA',
        value: 35
      }, {
        id: 'NG-YO',
        value: 36
      }, {
        id: 'NG-ZA',
        value: 37
      }
    ];

    // Set up heat legend
    const heatLegend = chart.createChild(am4maps.HeatLegend);
    heatLegend.series = polygonSeries;
    heatLegend.align = 'right';
    heatLegend.valign = 'bottom';
    heatLegend.width = am4core.percent(20);
    heatLegend.marginRight = am4core.percent(4);
    heatLegend.minValue = 0;
    heatLegend.maxValue = 40000000;

    // Set up custom heat map legend labels using axis ranges
    const minRange = heatLegend.valueAxis.axisRanges.create();
    minRange.value = heatLegend.minValue;
    minRange.label.text = 'Little';
    const maxRange = heatLegend.valueAxis.axisRanges.create();
    maxRange.value = heatLegend.maxValue;
    maxRange.label.text = 'A lot!';

    // Blank out internal heat legend value axis labels
    heatLegend.valueAxis.renderer.labels.template.adapter.add('text', (labelText) => {
      return '';
    });

    // Configure series tooltip
    const polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = '{name}: {value}';
    polygonTemplate.nonScalingStroke = true;
    polygonTemplate.strokeWidth = 0.5;

    // Create hover state and set alternative fill color
    const hs = polygonTemplate.states.create('hover');
    hs.properties.fill = am4core.color('#3DA5E4');
  }
  /*
  * function use retrun date name from droupdown
  * @param
  * @memberof DashboardComponent
  */
  findObjectByKey(value) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.dateRange.length; i++) {
      if (this.dateRange[i].id === value) {
        return this.dateRange[i].name;
      }
    }
    return 'Last Month';
  }
  /*
  * function use fetch Dashborda Data
  * @param roleName
  * @memberof DashboardComponent
  */
  getDashbordaData() {
    return new Promise((resolve, reject) => {
      this.isLoadingResults = true;
      const param = { roleName: this.usertype };
      this.DashbordApi.getreportTab(param).subscribe((res: DashbordApiResponce) => {
        console.log('res =getreportTab', res);
        this.isLoadingResults = false;
        if (res.success) {
          this.owlItems = res.data;
          if (this.owlItems.length > 0) {
            this.graphtag = this.owlItems[0].bargraphtag;
            this.bartitle = this.owlItems[0].title;
            resolve();
          }
        }
      }, (err) => {
        this.isLoadingResults = false;
        reject();
      });
    });
  }
  /*
  * function use for Changedate bargraph
  * @param
  * @memberof DashboardComponent
  */
  Changedate(event, type) {
    if (event.value !== null) {
      if (type === 'from') {
        this.from = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
      } else if (type === 'to') {
        this.to = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
      }
    } else {
      this.toast.error('Please Choose A Valid Date Format');
    }
  }
  /*
  * function use for Changedate Region donargraph
  * @param
  * @memberof DashboardComponent
  */
  ChangedateRegion(event, type) {
    if (event.value !== null) {
      if (type === 'from') {
        this.fromRegion = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
      } else if (type === 'to') {
        this.toRegion = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
      }
    } else {
      this.toast.error('Please Choose A Valid Date Format');
    }
  }
  /*
  * function use for filte rWith costomdate barchart
  * @param
  * @memberof DashboardComponent
  */
  filterWithcostomdatebarchart() {
    if (this.from !== '' && this.to !== '') {
      this.isLoadingResults = true;
      // const param = { startDate: this.from, endDate: this.to, graph_type: this.graphtag };
      const param = { startDate: this.from, endDate: this.to, graph_type: 'referred' };

      this.DashbordApi.getGraphData(param).subscribe((res: AdminDashboardGraphApiResponce) => {
        this.isLoadingResults = false;
        if (res.success) {
          this.setBarChart(res.data.date, res.data.data);
          this.datacount = 0;
          res.data.data.map((item, index) => {
            this.datacount = this.datacount + item;
          });
        }
      }, (err) => {
        this.isLoadingResults = false;
      });
    } else {
      this.toast.error('Please Choose Valid Date Range');
    }
  }
  /*
  * function use for filte rWith costomdate donarchart
  * @param
  * @memberof DashboardComponent
  */
  filterWithcostomdatedoughnutChartRegion() {
    if (this.fromRegion !== '' && this.toRegion !== '') {
      this.isLoadingResults = true;
      const param = { lga: this.Seletedlga, startDate: this.fromRegion, endDate: this.toRegion, roleName: this.usertype };
      this.DashbordApi.getstateWiseReportData(param).subscribe((res: StateWiseReportApiResponce) => {
        this.isLoadingResults = false;
        // console.log(res);
        if (res.success) {
          this.Regionwisedata = [];
          res.name.forEach((itm, i) => {
            this.Regionwisedata.push(Object.assign({}, { name: itm, value: res.dataAsNumber[i] }));
          });
          this.setdoughnutChart(res.name, res.data);
        }
      }, (err) => {
        this.isLoadingResults = false;
      });


    } else {
      this.toast.error('Please Choose Valid Date Range');
    }
  }
  /*
  * function use for date BarChart
  * @param
  * @memberof DashboardComponent
  */
  datechangedBarChart(date?) {
    let fromdate;
    let todate;
    this.datetitelbargraph = this.findObjectByKey(date);
    this.showDatepicker = false;
    if (date !== 5) {
      this.from = '';
      this.to = '';
    }
    if (date === 1) {
      fromdate = moment().subtract(7, 'day').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 2) {
      fromdate = moment().subtract(15, 'day').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 3) {
      fromdate = moment().subtract(30, 'day').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 4) {
      fromdate = moment().subtract(1, 'years').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 5) {
      this.showDatepicker = true;
    }
    if (date !== 5) {
      const param = { startDate: fromdate, endDate: todate, graph_type: this.graphtag };
      this.isLoadingResults = true;
      this.DashbordApi.getGraphData(param).subscribe((res: AdminDashboardGraphApiResponce) => {
        this.isLoadingResults = false;
        if (res.success) {
          this.setBarChart(res.data.date, res.data.data);
          this.datacount = 0;
          res.data.data.map((item, index) => {
            this.datacount = this.datacount + item;
          });
        }
      }, (err) => {
        this.isLoadingResults = false;
      });
    }



  }

  /*
  * function use for date doughnutChart
  * @param
  * @memberof DashboardComponent
  */
  datechangeddoughnutChart(date?) {
    let fromdate;
    let todate;
    this.datetiteldonorgraph = this.findObjectByKey(date);
    this.showDatepickerRegion = false;
    if (date !== 5) {
      this.fromRegion = '';
      this.toRegion = '';
    }
    if (date === 1) {
      fromdate = moment().subtract(7, 'day').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 2) {
      fromdate = moment().subtract(15, 'day').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 3) {
      fromdate = moment().subtract(30, 'day').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 4) {
      fromdate = moment().subtract(1, 'years').format('YYYY-MM-DD');
      todate = moment().format('YYYY-MM-DD');
    } else if (date === 5) {
      this.showDatepickerRegion = true;
    }
    if (date !== 5) {
      const param = { lga: this.Seletedlga, startDate: fromdate, endDate: todate, roleName: this.usertype };
      this.isLoadingResults = true;
      this.DashbordApi.getstateWiseReportData(param).subscribe((res: StateWiseReportApiResponce) => {
        this.isLoadingResults = false;
        // console.log(res);
        if (res.success) {
          // tslint:disable-next-line:prefer-const
          this.Regionwisedata = [];
          res.name.forEach((itm, i) => {
            this.Regionwisedata.push(Object.assign({}, { name: itm, value: res.dataAsNumber[i] }));
          });
          this.setdoughnutChart(res.name, res.data);
        }
      }, (err) => {
        this.isLoadingResults = false;
      });
    }
  }
  /*
  * function use for get img url from name
  * @param
  * @memberof DashboardComponent
  */
  getImgUrl(imagename) {
    if (imagename && imagename !== '') {
      return AppConst.API_BASE_URL + '/' + imagename;
    } else {
      return 'assets/images/icon/patients.png';
    }

  }
  /*
  * function use for set value BarChart
  * @param
  * @memberof DashboardComponent
  */
  setBarChart(label?, datas?) {

    this.barchartObj.barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true,
      legend: false,
      scales: {
        xAxes: [{
          barPercentage: 1,
          barThickness: 25,
          maxBarThickness: 35,
          minBarLength: 2,
          gridLines: {
            offsetGridLines: false,
            display: false
          }
        }]
      }
    };
    this.barchartObj.barChartLabels = label ? label : [];
    this.barchartObj.barChartType = 'bar';
    this.barchartObj.barChartLegend = true;
    this.barchartObj.barChartData = [
      {
        data: datas ? datas : [],
        backgroundColor: this.StaticColor,
        hoverBackgroundColor: this.hoberStaticColor
      }
    ];

  }
  /*
  * function use for set value doughnut Chart
  * @param
  * @memberof DashboardComponent
  */
  setdoughnutChart(label?, datas?) {
    this.doughnutChartObj.doughnutChartType = 'doughnut';
    this.doughnutChartObj.options = {
      responsive: true,
      legend: false,
      cutoutPercentage: 80,
    };
    this.doughnutChartObj.doughnutChartLabels = label ? label : [];
    this.doughnutChartObj.doughnutChartData = [{
      data: datas ? datas : [],
      backgroundColor: this.duelColor,
      hoverBackgroundColor: this.hoverduelColor
    }
    ];
  }
  colorchange() {

  }
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    // console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    // console.log(event, active);
  }
  /*
  * function use for map lga wise
  * @param
  * @memberof DashboardComponent
  */
  getArea(data) {

    // tslint:disable-next-line:triple-equals
    if (data == 1) {
      this.mapObj.b = false;
      this.mapObj.a = true;
      // tslint:disable-next-line:triple-equals
    } else if (data == 2) {
      this.mapObj.a = false;
      this.mapObj.b = true;
    } else {
      this.Seletedlga = data;
      // console.log(this.Seletedlga);
      this.selecteddateRangeRegion = 3;
      this.showDatepickerRegion = false;
      this.datechangeddoughnutChart(3);
    }
  }
  /*
  * function use for Onclick get  graph data from owlItems
  * @param
  * @memberof DashboardComponent
  */
  OnclickowlItems(param, index) {
    this.isLoadingResults = true;
    this.clickindex = index;
    if (typeof param.bargraphtag !== 'undefined') {
      this.graphtag = param.bargraphtag;
      this.bartitle = param.title;
      this.selecteddateRange = 3;
      this.showDatepicker = false;
      this.datechangedBarChart(3);
    }

  }
  /*
  * function use for fetch LGAWiseInformation Table
  * @param
  * @memberof DashboardComponent
  */
  getLGAWiseInformationTable() {
    const param = {
      roleName: this.usertype,
      page: 5,
      pageIndex: 0
    };
    this.isLoadingResults = true;
    this.DashbordApi.getLGAWiseInformationTable(param).subscribe((res: LgaWiseReportApiResponce) => {
      this.isLoadingResults = false;
      if (res.success) {
        this.LGAWiseInformation = res.data;
        // tslint:disable-next-line:triple-equals
        if (this.usertype != 'Generalist user') {
          this.dataSourcesuperadmin = new MatTableDataSource(this.LGAWiseInformation);
        } else {
          this.dataSource = new MatTableDataSource(this.LGAWiseInformation);
        }
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  /*
  * function use set router navigate
  * @param
  * @memberof DashboardComponent
  */
  goDetailsLGAWiseInformation() {
    this.router.navigate(['/post-auth/lga-wise-information']);
  }
  /*
  * function use set router navigate
  * @param
  * @memberof DashboardComponent
  */
  countClick(has: any, event) {
    // console.log(event);
    // tslint:disable-next-line:triple-equals
    if (event == 'presumptive' || event == 'Screened') {
      this.router.navigate(['/post-auth/screened-report']);
      // tslint:disable-next-line:triple-equals
    } else if (event == 'referred') {
      this.router.navigate(['/post-auth/referred-report']);
      // tslint:disable-next-line:triple-equals
    } else if (event == 'initiateTreatment') {
      this.router.navigate(['/post-auth/treatment-report']);
      // tslint:disable-next-line:triple-equals
    } else if (event == 'Inactive' || event == 'Active' || event == 'Alluser') {
      this.router.navigate(['/post-auth/user']);
      // tslint:disable-next-line:triple-equals
    } else if (event == 'confirmedPositive' || event == 'confirmedNegative') {
      this.router.navigate(['/post-auth/tb-confirmation-report']);
    }

  }
  /*
  * function use reset  map lga wise information
  * @param
  * @memberof DashboardComponent
  */
  refeshmap() {
    this.mapObj.b = false;
    this.mapObj.a = false;
    this.Seletedlga = '';
    this.selecteddateRangeRegion = 3;
    this.showDatepickerRegion = false;
    this.datechangeddoughnutChart(3);
  }
}
