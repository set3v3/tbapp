

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { ReportService } from 'src/app/services/apis/report.service';
import { AppConst } from 'src/app/app.constants';
import * as _ from 'lodash';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { ViewScreenedReportComponent } from './view-screened-report/view-screened-report.component';
import { DatePipe } from '@angular/common';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { PatientListApiResponce, PatientListInfo, PatientListExportReportApiResponce } from 'src/app/models/report';

@Component({
  selector: 'app-screened-report',
  templateUrl: './screened-report.component.html',
  styleUrls: ['./screened-report.component.scss'],
  providers: []
})

export class ScreenedReportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  ScreenedReportlisting: Array<PatientListInfo>;
  totalItem: number;
  page: number;
  pageindex: number;
  searchParam: any;
  viewstatus: boolean;
  bydefatseletedTB: any;
  bydefatseletedHIV: any;
  confrimfrom: any;
  confrimto: any;
  from: string;
  inputsearchTxt: any;
  to: string;
  TBList: Array<any>;
  HivList: Array<any>;
  todaydate: any;
  ShowDatepicker: boolean;
  displayedColumns = ['firstName', 'lastName', 'visitDate', 'phoneNumber', 'age', 'sex', 'tbstatus', 'hivStatus'];
  // displayedColumns = ['firstName', 'lastName', 'tbstatus', 'visitDate', 'phoneNumber', 'age', 'sex', 'hivStatus', 'view'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private consoleProvider: ConsoleProvider,
    private ReportApi: ReportService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
    private datePipe: DatePipe,
    private toast: ToastProvider,

  ) {
    this.isLoadingResults = false;
    this.ScreenedReportlisting = [];
    this.todaydate = this.datePipe.transform(new Date().setDate(new Date().getDate()), 'yyyy-MM-dd').toString();
    this.ShowDatepicker = false;
    this.TBList = AppConst.TB;
    this.HivList = AppConst.HIV;
    this.bydefatseletedTB = 2;
    this.bydefatseletedHIV = 3;
    this.inputsearchTxt = '';
    this.confrimfrom = '';
    this.confrimto = '';
    this.from = '';
    this.to = '';
    this.totalItem = 0;
    this.viewstatus = Permission.permission('screened report', 'view');
    if (this.viewstatus) {
      this.displayedColumns.push('view');
    }
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchReportListing(0);
  }
  /*
  * function use to open view Screend Report modal
  * @param object
  * @memberof ScreenedReportComponent
  */
  ViewScreendReport(object) {
    const dialogRef = this.dialog.open(ViewScreenedReportComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      data: object,
      width: '500px'
    });
  }
  /*
  * function use to pagenation
  * @param event pageIndex , pageIndex
  * @memberof ScreenedReportComponent
  */
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchReportListing(event.pageIndex, '', '', event.pageSize);
  }
  /*
  * function use to show date picker for filter
  * @param
  * @memberof ScreenedReportComponent
  */
  ShowDtefifeker() {
    this.ShowDatepicker = !this.ShowDatepicker;
    this.confrimfrom = '';
    this.confrimto = '';
    this.fetchReportListing(0);
  }
  /*
  * function use to search by name ,phonenumber ,lastname etc
  * @param filterValue
  * @memberof ScreenedReportComponent
  */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    // this.ScreenedReportlisting = [];
    this.inputsearchTxt = filterValue;
    this.fetchReportListing(0);
  }
  /*
  * function use to  fetch ReportListing
  * @param index ,frm ,to ,limit
  * @memberof ScreenedReportComponent
  */
  fetchReportListing(index?, frm?, to?, limit?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    this.searchParam = {
      searchText: this.inputsearchTxt,
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      startDate: (frm !== '') ? frm : '',
      endDate: (to !== '') ? to : '',
      hasTb: this.bydefatseletedTB,
      isHiv: this.bydefatseletedHIV
    };
    this.ReportApi.getPatienReportList(this.searchParam).subscribe((res: PatientListApiResponce) => {
      this.isLoadingResults = false;
      // this.from = '';
      // this.to = '';
      // this.consoleProvider.log('getUserList', res);
      if (res.success) {
        this.ScreenedReportlisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.ScreenedReportlisting);
        this.dataSource.sort = this.sort;
        this.ShowDatepicker = !this.ShowDatepicker;
        this.ShowDatepicker = !this.ShowDatepicker;
        // this.dataSource.paginator = this.paginator;
      }
      if (this.ScreenedReportlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  /*
  * function use to  downolad excel
  * @param hasTb ,isHiv ,startDate ,endDate ,searchText
  * @memberof ScreenedReportComponent
  */
  downloadExport() {
    this.isLoadingResults = true;
    // tslint:disable-next-line:max-line-length
    const data = {
      hasTb: this.bydefatseletedTB,
      isHiv: this.bydefatseletedHIV,
      startDate: this.confrimfrom,
      endDate: this.confrimto,
      searchText: this.inputsearchTxt
    };
    this.ReportApi.downloadScreenedReportExport(data).subscribe((res: PatientListExportReportApiResponce) => {
      this.isLoadingResults = false;
      if (res.success) {
        window.location.href = res.data;
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  /*
  * function use to  Tb status Change droupdown
  * @param
  * @memberof ScreenedReportComponent
  */
  TbChange(enevt) {
    if (typeof this.paginator !== 'undefined') {
      this.paginator.pageIndex = 0;
    }
    this.fetchReportListing(0);
  }
  /*
  * function use to  hiv status Change droupdown
  * @param
  * @memberof ScreenedReportComponent
  */
  HIVChange(enevt) {
    if (typeof this.paginator !== 'undefined') {
      this.paginator.pageIndex = 0;
    }
    this.fetchReportListing(0);
  }
  /*
  * function use to chnage  date
  * @param event type
  * @memberof ScreenedReportComponent
  */
  Change(event, type) {
    if (event.value) {
      if (type === 'from') {
        this.from = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
      } else if (type === 'to') {
        this.to = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
        // console.log(this.to);
      }
    } else {
      this.toast.error('Please Choose A Valid Date Format');
    }
  }
  /*
  * function use to filter with date
  * @param
  * @memberof ScreenedReportComponent
  */
  filterwithdate() {
    if (typeof this.paginator !== 'undefined') {
      this.paginator.pageIndex = 0;
    }
    if (this.from !== '' && this.to !== '') {
      this.confrimfrom = this.from;
      this.confrimto = this.to;
      this.fetchReportListing(0, this.from, this.to);
    } else {
      this.toast.error('Please Choose Valid Date Range');
    }
  }
}

