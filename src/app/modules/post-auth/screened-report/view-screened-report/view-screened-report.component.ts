import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ImeiApiResponse } from 'src/app/models/usermaster';



@Component({
  selector: 'app-view-screened-report',
  templateUrl: './view-screened-report.component.html',
  styleUrls: ['./view-screened-report.component.scss'],
  providers: []
})
export class ViewScreenedReportComponent implements OnInit {
  userInfo: any;
  showLoader: boolean;

  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewScreenedReportComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.userInfo = this.data;
    this.showLoader = false;
    this.consoleProvider.log('userInfo', this.userInfo);


  }
  ngOnInit() {

  }

}
