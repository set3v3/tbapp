import { NgModule } from '@angular/core';
import { ScreenedReportRouteModule } from './screened-report.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ScreenedReportComponent } from './screened-report.component';
import { ReportService } from 'src/app/services/apis/report.service';
import { ViewScreenedReportComponent } from './view-screened-report/view-screened-report.component';
import { MatSortModule } from '@angular/material';
import { DatePipe } from '@angular/common';
@NgModule({
  imports: [
    ScreenedReportRouteModule,
    SharedModule,
    MatSortModule,
  ],
  declarations: [
    ScreenedReportComponent,
    ViewScreenedReportComponent
  ],
  entryComponents: [
    ScreenedReportComponent,
    ViewScreenedReportComponent
  ],
  providers: [ReportService, DatePipe]
})
export class ScreenedReportModule { }
