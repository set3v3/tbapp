import { NgModule } from '@angular/core';
import { HelpSupportRouteModule } from './help-support.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSortModule } from '@angular/material';
import { HelpSupportComponent } from './help-support.component';
import { HelpSupportModalComponent } from './help-support-modal/help-support-modal.component';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { DatePipe } from '@angular/common';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { HelpSupportService } from 'src/app/services/apis/helps.service';
import { CryptoProvider } from 'src/app/services/crypto/crypto.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { ViewHelpSupportComponent } from './view-help-support/view-help-support.component';

@NgModule({
  imports: [
    HelpSupportRouteModule,
    SharedModule,
    MatSortModule,
    EcoFabSpeedDialModule,
  ],
  declarations: [
    HelpSupportComponent,
    HelpSupportModalComponent,
    ViewHelpSupportComponent
  ],
  entryComponents: [
    HelpSupportComponent,
    HelpSupportModalComponent,
    ViewHelpSupportComponent
  ],
  providers: [DatePipe, UserMasterService,
    HelpSupportService, CryptoProvider,
    UserStorageProvider]
})
export class HelpSupportModule { }
