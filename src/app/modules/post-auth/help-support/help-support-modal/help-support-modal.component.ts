import { Component, OnInit, Inject, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { array } from '@amcharts/amcharts4/core';
import { AppConst } from 'src/app/app.constants';
import { DatePipe } from '@angular/common';
import { UserMasterService } from 'src/app/services/apis/usermaster';
import { HelpSupportService } from 'src/app/services/apis/helps.service';
import { CryptoProvider } from 'src/app/services/crypto/crypto.service';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { AmazingTimePickerService } from 'amazing-time-picker';

@Component({
  selector: 'app-help-support-modal',
  templateUrl: './help-support-modal.html',
  styleUrls: ['./help-support-modal.scss']
})
export class HelpSupportModalComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  helpsupportform: FormGroup;
  showLoader: boolean;
  issuesourcelist: Array<any>;
  userTypeList: Array<any>;
  IssueTypeList: Array<any>;
  todaydate: string;
  languagelist: Array<any>;
  StateList: Array<any>;
  LgaListing: Array<any>;
  ModeList: Array<any>;
  imageUrl: Array<any>;
  DocMessage: string;
  filesToUpload: Array<File> = [];
  userInfo: any;
  filevaidation: boolean;
  MenuList: Array<any>;
  @Output() OnAddHelpSupport = new EventEmitter<any>(true);
  constructor(
    private authApi: AuthService,
    private dialogRef: MatDialogRef<HelpSupportModalComponent>,
    private toast: ToastProvider,
    private SupportApi: HelpSupportService,
    private datePipe: DatePipe,
    private atp: AmazingTimePickerService,
    private fileEncryption: CryptoProvider,
    private userStorage: UserStorageProvider,
    private UserMasterApi: UserMasterService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.showLoader = false;
    this.filevaidation = false;
    this.StateList = [];
    this.LgaListing = [];
    this.MenuList = [];
    this.imageUrl = [];
    this.DocMessage = '';
    this.userInfo = this.userStorage.get();
    // console.log(this.userInfo.data.email);
    this.issuesourcelist = AppConst.Issuesource;
    this.userTypeList = [];
    this.languagelist = AppConst.LANGUAGE;
    this.ModeList = AppConst.MODE;
    this.userTypeList = AppConst.APP_USER_TYPE;
    this.todaydate = this.datePipe.transform(new Date().setDate(new Date().getDate()), 'yyyy-MM-dd').toString();
    this.getLgaState();
  }

  ngOnInit() {
    this.buildhelpsupportform();
  }

  buildhelpsupportform() {
    this.helpsupportform = new FormGroup({
      Issuesource: new FormControl('App', [
        Validators.required,
      ]),
      UserType: new FormControl('', [
        Validators.required
      ]),
      Device: new FormControl(''),
      occurranceTime: new FormControl(''
      ),
      AndroidVersion: new FormControl('', [
        Validators.required
      ]),
      androidAppVersion: new FormControl('', [
        Validators.required
      ]),
      OccurranceDate: new FormControl('', [
        Validators.required
      ]),
      menuName: new FormControl('', [
        Validators.required
      ]),
      IssueDetails: new FormControl('', [
        Validators.required
      ]),
      UserregisteredPhoneno: new FormControl('', [
        Validators.required
      ]),
      language: new FormControl('English', [
        Validators.required
      ]),
      state: new FormControl('', [
        Validators.required
      ]),
      lga: new FormControl('', [
        Validators.required
      ]),
      ReportedBy: new FormControl(this.userInfo ? this.userInfo.data.firstName + ' ' + this.userInfo.data.surName : '', [
        Validators.required
      ])
    });
  }

  cutSeconds(time) {
    const timeArr = time.split(':');
    if (timeArr.length > 2) {
      time = timeArr[0] + ':' + timeArr[1];
    }

    return time;
  }

  convertTo12Hr(time) {
    time = this.cutSeconds(time);
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
  }
  openFromTimePicker() {
    // const hrsval24 = parsefloatthis.helpsupportform.get('occurranceTime').value;

    const options = {};
    const amazingTimePicker = this.atp.open(options);
    amazingTimePicker.afterClose().subscribe(time => {
      console.log(this.convertTo12Hr(time));
      this.helpsupportform.patchValue(
        {
          occurranceTime: this.convertTo12Hr(time)
        }
      );
    });
  }

  /*
  * function use api hit for  helpsupport porpose
  * @param
  * @memberof HelpSupportComponent
  */
  helpsupportSubmit(info) {
    this.filevaidation = false;
    const files: Array<File> = this.filesToUpload;
    console.log(files);
    if (this.helpsupportform.valid && files.length > 0) {
      const formData = new FormData();

      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < files.length; i++) {

        formData.append('image', files[i]);
      }
      formData.append('email', this.userInfo.data.email);
      formData.append('issueSource', info.Issuesource);
      formData.append('userType', info.UserType);
      formData.append('device', info.Device);
      formData.append('androidVersion', info.AndroidVersion);
      formData.append('androidAppVersion', info.androidAppVersion);
      formData.append('occuranceDate', info.OccurranceDate);
      formData.append('occurranceTime', info.occurranceTime);
      formData.append('menuName', info.menuName);
      formData.append('issueDetails', info.IssueDetails);
      formData.append('userRegisteredPhoneno', info.UserregisteredPhoneno);
      formData.append('language', info.language);
      formData.append('state', info.state);
      formData.append('lga', info.lga);
      formData.append('reportedBy', info.ReportedBy);
      this.showLoader = true;
      this.SupportApi.Submit(formData).subscribe((responseData: any) => {
        this.showLoader = false;
        if (responseData.success) {
          this.OnAddHelpSupport.emit(responseData);
          this.toast.success(responseData.message);
          this.dialogRef.close();
        }
      }, error => {
        this.showLoader = false;
      });
    } else {
      this.filevaidation = true;
    }

  }
  getLgaState() {
    this.UserMasterApi.getlgaState().subscribe((res: any) => {
      console.log(res);
      if (res.success) {
        this.StateList = res.data.addressDetails;
        console.log(this.StateList);
      }

      // this.LgaListing = AppConst.LGA_NAME;
    }, (err) => {
    });
  }
  StateChange(event) {
    const index = this.StateList.findIndex(x => x.State === event.value);
    if (index > -1) {
      this.LgaListing = this.StateList[index].LGA;
    }
    console.log(this.LgaListing);
  }
  onImagechange(event: any) {
    this.filevaidation = false;
    // tslint:disable-next-line:no-angle-bracket-type-assertion
    // this.filesToUpload = <Array<File>>event.target.files;
    // const files: Array<File> = this.filesToUpload;
    this.filesToUpload = [];
    for (let i = 0; i <= event.target.files.length - 1; i++) {
      const selectedFile = event.target.files[i];
      this.filesToUpload.push(selectedFile);
    }
    // tslint:disable-next-line:prefer-for-of
    this.DocMessage = '';
    const arry = [];
    this.imageUrl = [];
    if (this.filesToUpload.length < 4) {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.filesToUpload.length; i++) {

        const type = this.filesToUpload[i].type.split('/')[1];
        const size = this.filesToUpload[i].size / 1024;
        if (type === 'jpeg' || type === 'png' || type === 'jpg') {
          if (size < 2048) {
            const img = new Image();
            img.src = window.URL.createObjectURL(this.filesToUpload[i]);
            // console.log('img', files[i].size / 1024)
            const reader = new FileReader();
            img.onload = () => {
              window.URL.revokeObjectURL(img.src);
              reader.onload = (e: any) => {
                this.imageUrl.push(e.target.result);
                arry.push(this.filesToUpload[i].name);
                // console.log(e.target.result);
              };
              reader.readAsDataURL(this.filesToUpload[i]);
            };
          } else {

            this.toast.error('Upload Image With Max Size 2 Mb');
          }
        } else {
          this.toast.error('Uplaod Image Only');

        }
      }
      // console.log(this.imageUrl);
      this.DocMessage = arry.join(', ');
    } else {

      this.toast.error('You Can Add Max 3 images');
    }


  }
  deleteImge(index) {
    // const files: Array<File> = this.filesToUpload;
    // const index = this.imageUrl.findIndex(x => x === data);
    this.imageUrl.splice(index, 1);
    this.filesToUpload.splice(index, 1);
  }
  IssuesourceChange(value) {
    console.log(value);
    if (value === 'App') {
      this.userTypeList = AppConst.APP_USER_TYPE;
    } else {
      this.userTypeList = AppConst.USER_TYPE;
    }

  }
  SetManuNeme(value) {
    console.log(value);
    if (value === 'clinician') {
      this.MenuList = AppConst.CLINIC_MENU;
    } else if (value === 'independent') {
      this.MenuList = AppConst.IP_MENU;
    } else if (value === 'ppmv') {
      this.MenuList = AppConst.PPMVMENU;
    } else {
      this.MenuList = AppConst.LAB_MENU;
    }

  }
}

