

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { AppConst } from 'src/app/app.constants';
import { HelpSupportModalComponent } from './help-support-modal/help-support-modal.component';
import { HelpSupportService } from 'src/app/services/apis/helps.service';
import { ViewHelpSupportComponent } from './view-help-support/view-help-support.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-help-support',
  templateUrl: './help-support.component.html',
  styleUrls: ['./help-support.component.scss'],
  providers: []
})

export class HelpSupportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  supportlisting: Array<any>;
  totalItem: number;
  page: number;
  pageindex: number;
  Search: string;
  createtatus: boolean;
  viewstatus: boolean;
  pdfUrl: Array<any>;
  selettedpdf: string;
  displayedColumns = ['supportNumber', 'IssueSource', 'userType', 'occuranceDate', 'reportedBy', 'status'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    public dialog: MatDialog,
    private Permission: PermissionProvider,
    private SupportApi: HelpSupportService,
    private httpClient: HttpClient
  ) {
    this.isLoadingResults = false;
    this.supportlisting = [];
    this.pdfUrl = AppConst.PDFURL;
    this.selettedpdf = 'Independent';
    this.viewstatus = Permission.permission('Help & Support', 'view');
    this.createtatus = Permission.permission('Help & Support', 'create');
    if (this.viewstatus) {
      this.displayedColumns.push('view');
    }

    this.showMessage = '';
    this.Search = '';
  }

  ngOnInit() {
    this.fetchSupportListing(0);
  }
  openhelpsupportmodal() {
    const dialogRef = this.dialog.open(HelpSupportModalComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      width: '800px'
    });
    dialogRef.componentInstance.OnAddHelpSupport.subscribe((data: any) => {
      console.log('fetchSupportListing');
      this.fetchSupportListing(0);
    });
  }
  /*
  * function use to  pagenation
  * @param pageIndex, pageSize
  * @memberof RefferedReportComponent
  */
  getNext(event: PageEvent) {
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchSupportListing(event.pageIndex, event.pageSize);
  }
  fetchSupportListing(index?, limit?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    const param = {
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      supportNumber: this.Search
    };
    // console.log(param);
    this.SupportApi.list(param).subscribe((res: any) => {
      this.isLoadingResults = false;
      // console.log(res);
      if (res.success) {
        this.supportlisting = res.data.supportFormList;
        this.totalItem = res.data.totalCount;
        this.dataSource = new MatTableDataSource(this.supportlisting);
        this.dataSource.sort = this.sort;
      }
      if (this.supportlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  downloadExport() {
  }
  viewHelpsupport(object) {
    const dialogRef = this.dialog.open(ViewHelpSupportComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      width: '700px',
      data: object,
    });
  }
  applyFilter(filterValue: string) {
    this.Search = filterValue.trim().toUpperCase();  // Remove whitespace
    this.fetchSupportListing(0);
  }
  downloadPdf() {
    const index = this.pdfUrl.findIndex(x => x.name === this.selettedpdf);
    if (index > -1) {
      const link = AppConst.IMG_BASE_URL + '/public/uploads/' + this.pdfUrl[index].filename;
      // window.location.href = link;
      window.open(link, 'blank');
      // return this.httpClient.get(link, { responseType: 'blob' });
    }
  }
  pdfChange(event) {
    console.log(event);

  }
}

