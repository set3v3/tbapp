import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { DatePipe } from '@angular/common';
import { AppConst } from 'src/app/app.constants';



@Component({
  selector: 'app-view-help-support',
  templateUrl: './view-help-support.component.html',
  styleUrls: ['./view-help-support.component.scss'],
  providers: []
})
export class ViewHelpSupportComponent implements OnInit {
  Messagedetails: any;
  showLoader: boolean;
  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewHelpSupportComponent>,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.showLoader = true;
    this.Messagedetails = this.data;
    this.Messagedetails.occuranceDate = this.datePipe.transform((this.Messagedetails.occuranceDate), 'yyyy-MM-dd').toString();
    console.log(this.Messagedetails);

  }
  ngOnInit() {
    this.showLoader = false;
  }
  getImgUrl(imagename) {
    if (imagename && imagename !== '') {
      return AppConst.IMG_BASE_URL + '/' + imagename;
    }
  }
}
