import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';




@Component({
  selector: 'app-view-reffered-report',
  templateUrl: './view-reffered-report.component.html',
  styleUrls: ['./view-reffered-report.component.scss'],
  providers: []
})
export class ViewRefferedReportComponent implements OnInit {
  refferedReportInfo: any;
  showLoader: boolean;

  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewRefferedReportComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.refferedReportInfo = this.data;
    this.showLoader = false;
    this.consoleProvider.log('refferedReportInfo', this.refferedReportInfo);


  }
  ngOnInit() {

  }

}
