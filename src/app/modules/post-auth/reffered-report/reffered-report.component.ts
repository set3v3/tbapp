

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserlistApiResponse } from 'src/app/models/usermaster';
import { ReportService } from 'src/app/services/apis/report.service';
import * as _ from 'lodash';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { ViewRefferedReportComponent } from './view-reffered-report/view-reffered-report.component';
import { AppConst } from 'src/app/app.constants';
import { IndependentPatientReportApiResponce, ExportReportApiResponce } from 'src/app/models/report';

@Component({
  selector: 'app-reffered-report',
  templateUrl: './reffered-report.component.html',
  styleUrls: ['./reffered-report.component.scss'],
  providers: []
})

export class RefferedReportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  RefferedReportlisting: Array<any>;
  totalItem: number;
  page: number;
  pageindex: number;
  searchParam: any;
  viewstatus: boolean;
  bydefatseletedStatus: any;
  ReferralStatus: any;
  displayedColumns = ['firstName', 'middleName', 'lastName', 'phoneNumber', 'age', 'sex', 'referred'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private consoleProvider: ConsoleProvider,
    private ReportApi: ReportService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
  ) {
    this.isLoadingResults = false;
    this.RefferedReportlisting = [];
    this.bydefatseletedStatus = 2;
    this.ReferralStatus = AppConst.RStatus;
    this.viewstatus = Permission.permission('referred report', 'view');
    if (this.viewstatus) {
      this.displayedColumns.push('view');
    }
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchReportListing(0);
  }
  /*
  * function use to  open view Reffered Report  modal
  * @param object
  * @memberof RefferedReportComponent
  */
  ViewRefferedReport(object) {
    const dialogRef = this.dialog.open(ViewRefferedReportComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      data: object,
      width: '500px'
    });
  }
  /*
  * function use to  pagenation
  * @param pageIndex, pageSize
  * @memberof RefferedReportComponent
  */
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchReportListing(event.pageIndex, event.pageSize);
  }

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim(); // Remove whitespace
  //   filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
  //   this.ScreenedReportlisting = [];
  //   this.fetchUserListing(0, '', filterValue);
  // }
  /*
  * function use to  fetch Report Listing
  * @param index, limit ,searchTxt
  * @memberof RefferedReportComponent
  */
  fetchReportListing(index?, limit?, searchTxt?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    this.searchParam = {
      // userType: (searchTxt) ? searchTxt : '',
      SerachTag: this.bydefatseletedStatus,
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      report_type: 'referred'
    };
    this.ReportApi.getReportList(this.searchParam).subscribe((res: IndependentPatientReportApiResponce) => {
      this.isLoadingResults = false;
      // this.consoleProvider.log('getUserList', res);
      if (res.success) {

        this.RefferedReportlisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.RefferedReportlisting);
        this.dataSource.sort = this.sort;
        // console.log(this.dataSource.paginator);
        // this.dataSource.paginator = this.paginator;
      }
      if (this.RefferedReportlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  /*
  * function use to download report excel
  * @param index, limit ,searchTxt
  * @memberof RefferedReportComponent
  */
  downloadExport() {
    this.isLoadingResults = true;
    const param = { report_type: 'referred' };
    this.ReportApi.downloadReportExport(param).subscribe((res: ExportReportApiResponce) => {
      this.isLoadingResults = false;
      if (res.success) {
        window.location.href = res.data;
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  /*
  * function use change value of filter droupdown
  * @param index, limit ,searchTxt
  * @memberof RefferedReportComponent
  */
  StatusChange(param) {
    // console.log(param);
    if (typeof this.paginator !== 'undefined') {
      this.paginator.pageIndex = 0;
    }
    this.bydefatseletedStatus = param.value;
    this.fetchReportListing(0);

  }
}

