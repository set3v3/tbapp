import { NgModule } from '@angular/core';
import { RefferedReportRouteModule } from './reffered-report.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ReportService } from 'src/app/services/apis/report.service';
import { RefferedReportComponent } from './reffered-report.component';
import { ViewRefferedReportComponent } from './view-reffered-report/view-reffered-report.component';
import { MatSortModule } from '@angular/material';

@NgModule({
  imports: [
    RefferedReportRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    RefferedReportComponent,
    ViewRefferedReportComponent
  ],
  entryComponents: [
    RefferedReportComponent,
    ViewRefferedReportComponent
  ],
  providers: [ReportService]
})
export class RefferedReportModule { }
