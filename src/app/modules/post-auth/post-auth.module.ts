import { NgModule } from '@angular/core';
// import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PostAuthComponent } from './post-auth.component';
import { PostAuthRouteModule } from './post-auth.routes.module';
import { ComponentsModule } from '../../components/components.module';
import { AppMaterialModule } from '../../app.material.module';
import { AuthService } from '../../services/apis/auth.service';
import { SharedModule } from '../../shared/shared.module';
import { CryptoProvider } from '../../services/crypto/crypto.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangepasswordComponent } from './change-password/change-password.component';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { OwlModule } from 'ngx-owl-carousel';
import { ChartsModule } from 'ng2-charts';
import { PermissionProvider } from 'src/app/services/permission/permission.service';

@NgModule({
  imports: [
    PostAuthRouteModule,
    ComponentsModule,
    AppMaterialModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    FormsModule,
    OwlModule,
    ChartsModule
  ],
  declarations: [
    PostAuthComponent,
    DashboardComponent,
    ChangepasswordComponent
  ],
  entryComponents: [
    ChangepasswordComponent
  ],

  providers: [AuthService, PermissionProvider]
})
export class PostAuthModule { }
