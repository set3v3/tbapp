import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';



@Component({
  selector: 'app-view-facility',
  templateUrl: './view-facility.component.html',
  styleUrls: ['./view-facility.component.scss'],
  providers: []
})
export class ViewFacilityComponent implements OnInit {
  facilityInfo: any;
  showLoader: boolean;
  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewFacilityComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.facilityInfo = this.data;
    this.showLoader = false;
    this.consoleProvider.log('facilityInfo', this.facilityInfo);
  }
  ngOnInit() {
  }
}
