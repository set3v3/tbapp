import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { FileValidator } from '../../../../directives/validators/file-validation';
import { MatStepper } from '@angular/material';
import * as _ from 'lodash';

@Component({
  selector: 'app-addbulk-facility',
  templateUrl: './bulk-facility-upload.component.html',
  styleUrls: ['./bulk-facility-upload.component.scss'],
  providers: []
})
export class AddBulkFacilityComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  @Output() facilityonBulkadd = new EventEmitter<any>(true);
  showLoader: boolean;
  fileFormGroup: FormGroup;
  imageFormGroup: FormGroup;
  progressBarvalue: number;
  storeListing: Array<any>;
  userInfo: any;
  uploadFileName: string;
  constructor(
    private toast: ToastProvider
  ) {
    this.uploadFileName = '';
    this.showLoader = false;
    this.progressBarvalue = 0;
    this.storeListing = [];
  }
  ngOnInit() {
    this.fileFormGroup = new FormGroup({
      file: new FormControl('', [FileValidator.validateExcelfile])
    });
  }
  moveNext(stepper: MatStepper) {
    if (this.fileFormGroup.valid) {
      stepper.selected.editable = false;
      const fileBrowser = this.fileInput.nativeElement;
      if (fileBrowser.files && fileBrowser.files[0]) {
        const formData = new FormData();
        formData.append('file_name', fileBrowser.files[0]);
        this.uploadFileName = '';
        stepper.next();
        // this.menuApi.uploadExcelsheet(formData).subscribe((response) => {
        //   this.showLoader = true;
        //   this.progressBarvalue = Math.round(100 * response.loaded / response.total);
        //   if (response.body) {
        //     if (response.body.status === 200) {
        //       this.showLoader = false;
        //       if (response.body.data.errors.length > 0) {
        //         this.toast.error(response.body.data.errors[0], 10000);
        //       } else {
        //         this.uploadFileName = '';
        //         stepper.next();
        //       }

        //     }
        //   }
        // }, (errorData: any) => {
        //   this.showLoader = false;
        // });

      }
    }
  }
  onFilechange(event: any) {
    const allowedType = ['xlsx', 'xls'];
    this.uploadFileName = '(' + event.target.files[0].name + ')';
    if (allowedType.indexOf(this.getFileExtention(event.target.files[0].name)) >= 0) {
    } else {
      event.target.value = '';
      this.toast.error('Uplaod Excel sheet Only');
    }
  }
  menufacilityDone() {
    this.facilityonBulkadd.emit(true);
  }
  getFileExtention(fileName: string): string {
    return fileName.substr((fileName.lastIndexOf('.') + 1)).toLowerCase();
  }

}
