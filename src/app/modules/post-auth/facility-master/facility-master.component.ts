
import { AppConst } from '../../../app.constants';
import { Component, OnInit, ViewChild } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, PageEvent } from '@angular/material';
import { ViewFacilityComponent } from './view-facility/view-facility.component';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { AddBulkFacilityComponent } from './bulk-facility-upload/bulk-facility-upload.component';
import { FacilityMasterService } from 'src/app/services/apis/facility.service';

@Component({
  selector: 'app-facility-master',
  templateUrl: './facility-master.component.html',
  styleUrls: ['./facility-master.component.scss'],
  providers: []
})
export class FacilityMasterComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  facilitylisting: Array<any>;
  totalItem: number;
  Imageurl: string;
  fixVar: boolean;
  open: boolean;
  spin: boolean;
  direction: string;
  animationMode: string;
  displayedColumns = ['firstName', 'surName', 'view'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private consoleProvider: ConsoleProvider,
    private FacilityProvider: FacilityMasterService,

    public dialog: MatDialog,
  ) {
    this.isLoadingResults = false;
    this.fixed = false;
    this.open = false;
    this.spin = false;
    this.direction = 'up';
    this.animationMode = 'fling';
    this.facilitylisting = [];
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchFacilityListing(0);
  }
  viewFacility(object) {
    const dialogRef = this.dialog.open(ViewFacilityComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      data: object
    });
  }
  AddbulkFacility() {
    const dialogRef = this.dialog.open(AddBulkFacilityComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: ''
    });
  }
  get fixed(): boolean {
    return this.fixVar;
  }

  set fixed(fixed: boolean) {
    this.fixVar = fixed;
    if (this.fixVar) {
      this.open = true;
    }
  }

  public doAction(event: any) {
  }
  fetchFacilityListing(index?, limit?, searchTxt?) {
    this.isLoadingResults = true;
    const param = {
      userType: (searchTxt) ? searchTxt : '',
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0
    };
    this.FacilityProvider.getFacilityList(param).subscribe((res: any) => {
      this.isLoadingResults = false;
      // this.consoleProvider.log('getUserList', res);
      if (res.success) {
        this.facilitylisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.facilitylisting);
        // this.dataSource.paginator = this.paginator;
      }
      if (this.facilitylisting.length <= 0) {
        this.showMessage = 'No Facility Avaliable';
      }

    }, (err) => {
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.fetchFacilityListing(event.pageIndex, event.pageSize);
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    // filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.facilitylisting = [];
    this.fetchFacilityListing(0, '', filterValue);
  }
}

