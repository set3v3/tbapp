import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { FacilityMasterComponent } from './facility-master.component';
import { FacilityMasterRouteModule } from './facility-master.routes.module';
import { ViewFacilityComponent } from './view-facility/view-facility.component';
import { EcoFabSpeedDialModule } from '@ecodev/fab-speed-dial';
import { MatStepperModule } from '@angular/material';
import { AddBulkFacilityComponent } from './bulk-facility-upload/bulk-facility-upload.component';
import { FacilityMasterService } from 'src/app/services/apis/facility.service';
@NgModule({
  imports: [
    FacilityMasterRouteModule,
    SharedModule,
    MatStepperModule,
    EcoFabSpeedDialModule,

  ],
  declarations: [
    FacilityMasterComponent,
    ViewFacilityComponent,
    AddBulkFacilityComponent
  ],
  entryComponents: [
    FacilityMasterComponent,
    ViewFacilityComponent,
    AddBulkFacilityComponent
  ],
  providers: [FacilityMasterService]
})
export class FacilityMasterModule { }
