import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportService } from 'src/app/services/apis/report.service';
import { MatSortModule } from '@angular/material';
import { ReferralPatientsDetailsComponent } from './referral-patients-details.component';
import { ReferralPatientsRouteModule } from './referral-patients-details.routes.module';


@NgModule({
  imports: [
    ReferralPatientsRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    ReferralPatientsDetailsComponent
  ],
  entryComponents: [
    ReferralPatientsDetailsComponent
  ],
  providers: [ReportService]
})
export class ReferralPatientsDetailsModule { }
