

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { ReportService } from 'src/app/services/apis/report.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-referral-patients-details',
  templateUrl: './referral-patients-details.component.html',
  styleUrls: ['./referral-patients-details.component.scss'],
  providers: []
})
export class ReferralPatientsDetailsComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  ReferralPatientsDetailslisting: Array<any>;
  totalItem: number;
  page: number;
  pageindex: number;
  searchParam: any;
  userID: any;
  displayedColumns = ['patient_record_no', 'firstName',
    'middleName', 'lastName', 'Phone_number', 'state', 'current_status'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private consoleProvider: ConsoleProvider,
    private ReportApi: ReportService,
    public dialog: MatDialog,
    private router: Router,
  ) {
    this.isLoadingResults = false;
    this.ReferralPatientsDetailslisting = [];
    this.showMessage = '';
    // tslint:disable-next-line:max-line-length
    this.userID = this.router.getCurrentNavigation().extras.hasOwnProperty('state') ? this.router.getCurrentNavigation().extras.state.id : null;
    console.log(this.userID);
    if (!this.userID) {
      this.router.navigate(['post-auth/no-access']);
      return;
    }
    this.fetchReferralPatientsDetails();
  }

  ngOnInit() {

  }
  ViewReferralPatientsDetails(object) {
    // const dialogRef = this.dialog.open(ViewTbConfirmationReportComponent, {
    //   panelClass: 'dialog-xs',
    //   disableClose: true,
    //   data: object,
    //   width: '500px'
    // });
  }
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchReferralPatientsDetails(event.pageIndex, event.pageSize);
  }


  fetchReferralPatientsDetails(index?, limit?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    const param = {
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      refferbyuserId: this.userID
    };
    this.ReportApi.incentiveDetais(param).subscribe((res: any) => {
      this.isLoadingResults = false;
      if (res.success) {
        this.ReferralPatientsDetailslisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.ReferralPatientsDetailslisting);
        this.dataSource.sort = this.sort;
        // this.dataSource.paginator = this.paginator;
      }
      if (this.ReferralPatientsDetailslisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  goback() {
    this.router.navigate(['post-auth/referral-incentive-report']);
  }

}

