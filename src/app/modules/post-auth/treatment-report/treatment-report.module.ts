import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportService } from 'src/app/services/apis/report.service';
import { TreatmentReportComponent } from './treatment-report.component';
import { TreatmentReportRouteModule } from './treatment-report.routes.module';
import { ViewTreatmentReportComponent } from './view-treatment-report/view-treatment-report.component';
import { MatSortModule } from '@angular/material';
;

@NgModule({
  imports: [
    TreatmentReportRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    TreatmentReportComponent,
    ViewTreatmentReportComponent
  ],
  entryComponents: [
    TreatmentReportComponent,
    ViewTreatmentReportComponent
  ],
  providers: [ReportService]
})
export class TreatmentReportModule { }
