

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserlistApiResponse } from 'src/app/models/usermaster';
import { ReportService } from 'src/app/services/apis/report.service';
import * as _ from 'lodash';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { ViewTreatmentReportComponent } from './view-treatment-report/view-treatment-report.component';
import { AppConst } from 'src/app/app.constants';
import { IndependentPatientReportApiResponce, ExportReportApiResponce } from 'src/app/models/report';

@Component({
  selector: 'app-treatment-report',
  templateUrl: './treatment-report.component.html',
  styleUrls: ['./treatment-report.component.scss'],
  providers: []
})
export class TreatmentReportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  TreatmentReportlisting: Array<any>;
  totalItem: number;
  page: number;
  pageindex: number;
  searchParam: any;
  viewstatus: boolean;
  TreatmentInitiated: any;
  bydefatseletedTreatmentInitiated: any;
  displayedColumns = ['firstName', 'middleName', 'lastName', 'phoneNumber', 'age', 'sex', 'initiateTreatment'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private consoleProvider: ConsoleProvider,
    private ReportApi: ReportService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
  ) {
    this.isLoadingResults = false;
    this.TreatmentReportlisting = [];
    this.bydefatseletedTreatmentInitiated = 2;
    this.TreatmentInitiated = AppConst.TreatmentInitiated;
    this.viewstatus = Permission.permission('treatment report', 'view');
    if (this.viewstatus) {
      this.displayedColumns.push('view');
    }
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchReportListing(0);
  }
  /*
  * function use to open view  TreatmentReport Details modal
  * @param details arary
  * @memberof TreatmentReportComponent
  */
  ViewTreatmentReport(object) {
    const dialogRef = this.dialog.open(ViewTreatmentReportComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      data: object,
      width: '500px'
    });
  }
  /*
  * function use pagenation in next page
  * @param event
  * @memberof TreatmentReportComponent
  */
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchReportListing(event.pageIndex, event.pageSize);
  }
  /*
  * function use search data
  * @param filterValue
  * @memberof TreatmentReportComponent
  */
  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim(); // Remove whitespace
  //   this.ScreenedReportlisting = [];
  //   this.fetchUserListing(0, '', filterValue);
  // }
  /*
  * function use fetch Report Listing
  * @param index limit searchTxt
  * @memberof TreatmentReportComponent
  */
  fetchReportListing(index?, limit?, searchTxt?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    this.searchParam = {
      SerachTag: this.bydefatseletedTreatmentInitiated,
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      report_type: 'initiateTreatment'
    };
    this.ReportApi.getReportList(this.searchParam).subscribe((res: IndependentPatientReportApiResponce) => {
      this.isLoadingResults = false;
      // this.consoleProvider.log('getUserList', res);
      if (res.success) {
        this.TreatmentReportlisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.TreatmentReportlisting);
        this.dataSource.sort = this.sort;
        // this.dataSource.paginator = this.paginator;
      }
      if (this.TreatmentReportlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      // console.log('api_http-------errr', err);
    });
  }

  /*
  * function use download exal report
  * @param
  * @memberof TreatmentReportComponent
  */
  downloadExport() {
    this.isLoadingResults = true;
    const param = { report_type: 'initiateTreatment' };
    this.ReportApi.downloadReportExport(param).subscribe((res: ExportReportApiResponce) => {
      this.isLoadingResults = false;
      if (res.success) {
        window.location.href = res.data;
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  /*
  * function use filter nagative posetive filter report
  * @param
  * @memberof TreatmentReportComponent
  */
  InitiatedStatusChange(param) {
    // console.log(this.paginator);
    if (typeof this.paginator !== 'undefined') {
      this.paginator.pageIndex = 0;
    }
    // console.log(param);
    this.bydefatseletedTreatmentInitiated = param.value;
    this.fetchReportListing(0);
  }
}

