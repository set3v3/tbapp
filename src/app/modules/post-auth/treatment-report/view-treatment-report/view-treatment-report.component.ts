import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';




@Component({
  selector: 'app-view-treatment-report',
  templateUrl: './view-treatment-report.component.html',
  styleUrls: ['./view-treatment-report.component.scss'],
  providers: []
})
export class ViewTreatmentReportComponent implements OnInit {
  treatmentReportInfo: any;
  showLoader: boolean;

  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewTreatmentReportComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.treatmentReportInfo = this.data;
    this.showLoader = false;
    this.consoleProvider.log('TreatmentReportInfo', this.treatmentReportInfo);


  }
  ngOnInit() {

  }

}
