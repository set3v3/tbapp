import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';




@Component({
  selector: 'app-view-presumptive-not-referred-report',
  templateUrl: './view-presumptive-not-referred-report.component.html',
  styleUrls: ['./view-presumptive-not-referred-report.component.scss'],
  providers: []
})
export class ViewPresumptiveNotRefferedReportComponent implements OnInit {
  refferedReportInfo: any;
  showLoader: boolean;

  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewPresumptiveNotRefferedReportComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.refferedReportInfo = this.data;
    this.showLoader = false;
    this.consoleProvider.log('refferedReportInfo', this.refferedReportInfo);


  }
  ngOnInit() {

  }

}
