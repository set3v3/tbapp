import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportService } from 'src/app/services/apis/report.service';
import { PresumptiveNotReferredReportRouteModule } from './presumptive-not-referred-report.routes.module';
import { PresumptiveNotReferredReportComponent } from './presumptive-not-referred-report.component';
import {
  ViewPresumptiveNotRefferedReportComponent
} from './view-presumptive-not-referred-report/view-presumptive-not-referred-report.component';
import { MatSortModule } from '@angular/material';

@NgModule({
  imports: [
    PresumptiveNotReferredReportRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    PresumptiveNotReferredReportComponent,
    ViewPresumptiveNotRefferedReportComponent

  ],
  entryComponents: [
    PresumptiveNotReferredReportComponent,
    ViewPresumptiveNotRefferedReportComponent
  ],
  providers: [ReportService]
})
export class PresumptiveNotReferredReportModule { }
