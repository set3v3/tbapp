import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportService } from 'src/app/services/apis/report.service';
import { MatSortModule } from '@angular/material';
import { ReferralIncentiveReportComponent } from './referral-incentive-report.component';
import { ReferralIncentiveReportRouteModule } from './referral-incentive-report.routes.module';
import { DatePipe } from '@angular/common';
import { UserMasterService } from 'src/app/services/apis/usermaster';

@NgModule({
  imports: [
    ReferralIncentiveReportRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    ReferralIncentiveReportComponent
  ],
  entryComponents: [
    ReferralIncentiveReportComponent
  ],
  providers: [ReportService, DatePipe, UserMasterService]
})
export class ReferralIncentiveReportModule { }
