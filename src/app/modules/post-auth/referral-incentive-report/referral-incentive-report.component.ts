

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { ReportService } from 'src/app/services/apis/report.service';
import * as _ from 'lodash';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { AppConst } from 'src/app/app.constants';
import { ExportReportApiResponce } from 'src/app/models/report';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { DatePipe } from '@angular/common';
import { UserMasterService } from 'src/app/services/apis/usermaster';

import { Router } from '@angular/router';

@Component({
  selector: 'app-referral-incentive-report',
  templateUrl: './referral-incentive-report.component.html',
  styleUrls: ['./referral-incentive-report.component.scss'],
  providers: []
})
export class ReferralIncentiveReportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  ReferralIncentivelisting: Array<any>;
  totalItem: number;
  page: number;
  ShowDatepicker: boolean;
  pageindex: number;
  from: string;
  to: string;
  todaydate: string;
  viewstatus: boolean;
  bydefatseletedlga: any;
  bydefatseletedState: any;
  LgaListing: Array<any>;
  StateList: Array<any>;
  SelectedLga: string;
  SelectedState: string;
  bydefatseleteduserType: any;
  Usertype: any;
  selecteduser: any;
  searchTxt: string;
  displayedColumns = ['firstName', 'surName', 'State', 'userType', 'incentiveAmont',
    'noOfTotalRefPatients', 'noOfCompltRefPatients', 'noOfIncompleteRefPatients', 'view'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private consoleProvider: ConsoleProvider,
    private ReportApi: ReportService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
    private toast: ToastProvider,
    private datePipe: DatePipe,
    private UserMasterApi: UserMasterService,
    private router: Router,

  ) {
    this.isLoadingResults = false;
    this.ReferralIncentivelisting = [];
    this.bydefatseletedlga = 0;
    this.bydefatseletedState = 0;
    this.ShowDatepicker = false;
    this.StateList = [];
    this.LgaListing = [];
    this.SelectedLga = '';
    this.SelectedState = '';
    this.from = '';
    this.bydefatseleteduserType = 0;
    this.selecteduser = '';
    this.searchTxt = '';
    this.Usertype = AppConst.MOBILE_USER_TYPE;
    this.todaydate = this.datePipe.transform(new Date().setDate(new Date().getDate()), 'yyyy-MM-dd').toString();
    this.to = '';
    this.viewstatus = Permission.permission('referral incentive report', 'view');
    if (this.viewstatus) {
      this.displayedColumns.push('view');
    }
    this.showMessage = '';
    this.getLgaState();
  }

  ngOnInit() {

    this.fetchReportListing(0);
  }

  ShowDtefifeker() {
    this.ShowDatepicker = !this.ShowDatepicker;
    this.from = '';
    this.to = '';
    this.fetchReportListing(0);
  }
  filterwithdate() {
    if (typeof this.paginator !== 'undefined') {
      this.paginator.pageIndex = 0;
    }
    if (this.from !== '' && this.to !== '') {
      this.fetchReportListing(0);
    } else {
      this.toast.error('Please Choose Valid Date Range');
    }
  }
  Change(event, type) {
    if (event.value) {
      if (type === 'from') {
        this.from = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
      } else if (type === 'to') {
        this.to = this.datePipe.transform(event.value, 'yyyy-MM-dd').toString();
        // console.log(this.to);
      }
    } else {
      this.toast.error('Please Choose A Valid Date Format');
    }
  }
  /*
  * function use to open view  ReferralIncentiveReport Details modal
  * @param details arary
  * @memberof ReferralIncentiveReportComponent
  */
  ViewReferralIncentiveReport(object) {
    this.router.navigate(['post-auth/referral-patients-details'], { state: { id: object.UserId } });
  }

  // * function use pagenation in next page
  //   * @param event
  //     * @memberof ReferralIncentiveReportComponent
  //       * /
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchReportListing(event.pageIndex, event.pageSize);
  }
  /*
  * function use search data
  * @param filterValue
  * @memberof ReferralIncentiveReportComponent
  */
  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim(); // Remove whitespace
  //   this.ReferralIncentivelisting = [];
  //   this.searchTxt = filterValue;
  //   this.fetchReportListing(0);
  // }

  // * function use fetch Report Listing
  // * @param index limit searchTxt
  // * @memberof ReferralIncentiveReportComponent
  // */
  fetchReportListing(index?, limit?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    const param = {
      // SerachTag: this.searchTxt ? this.searchTxt : '',
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      State: this.SelectedState,
      LGA: this.SelectedLga,
      userType: this.selecteduser,
      startDate: (this.from !== '') ? this.from : '',
      endDate: (this.to !== '') ? this.to : '',
    };
    console.log(JSON.stringify(param));
    this.ReportApi.getincentiveReportList(param).subscribe((res: any) => {
      this.isLoadingResults = false;
      console.log(res);
      if (res.success) {
        this.ReferralIncentivelisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.ReferralIncentivelisting);
        this.dataSource.sort = this.sort;
        this.ShowDatepicker = !this.ShowDatepicker;
        this.ShowDatepicker = !this.ShowDatepicker;
        // this.dataSource.paginator = this.paginator;
      }
      if (this.ReferralIncentivelisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      // console.log('api_http-------errr', err);
    });
  }

  /*
  * function use download exal report
  * @param
  * @memberof TreatmentReportComponent
  */
  downloadExport() {
    // this.isLoadingResults = true;
    // const param = { report_type: 'initiateTreatment' };
    // this.ReportApi.downloadReportExport(param).subscribe((res: ExportReportApiResponce) => {
    //   this.isLoadingResults = false;
    //   if (res.success) {
    //     window.location.href = res.data;
    //   }
    // }, (err) => {
    //   this.isLoadingResults = false;
    // });
  }
  getLgaState() {
    this.UserMasterApi.getlgaState().subscribe((res: any) => {
      console.log(res);
      if (res.success) {
        this.StateList = res.data.addressDetails;
      }
    }, (err) => {
    });
  }
  StateChange(event) {
    // console.log(event);
    if (event.value === '0') {
      this.LgaListing = [];
      this.SelectedState = '';
      this.SelectedLga = '';
      this.ReferralIncentivelisting = [];
      this.fetchReportListing(0);

    } else {
      this.LgaListing = event.value.LGA;
      this.SelectedState = event.value.State;
      this.SelectedLga = '';
      this.ReferralIncentivelisting = [];
      this.fetchReportListing(0);
    }

  }
  LgaChange(event) {
    if (event.value === '0') {
      this.SelectedLga = '';
      this.ReferralIncentivelisting = [];
      this.fetchReportListing(0);

    } else {
      this.SelectedLga = event.value;
      this.ReferralIncentivelisting = [];
      this.fetchReportListing(0);
    }
  }
  usertypechange(event) {
    if (event.value === 0) {
      this.selecteduser = '';
      this.ReferralIncentivelisting = [];
      this.fetchReportListing(0);

    } else {
      this.selecteduser = event.value;
      this.ReferralIncentivelisting = [];
      this.fetchReportListing(0);
    }

  }
}

