
import { AppConst } from '../../../app.constants';
import { Component, OnInit } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { AddRoleComponent } from './add-role/add-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { DeleteRoleComponent } from './delete-role/delete-role.component';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { RoleListingApiResponce } from 'src/app/models/role';

@Component({
  selector: 'app-role-master',
  templateUrl: './role-master.component.html',
  styleUrls: ['./role-master.component.scss'],
  providers: []
})
export class RoleMasterComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  RoleListing: Array<any>;
  createtatus: boolean;
  editstatus: boolean;
  constructor(
    private consoleProvider: ConsoleProvider,
    private RoleApi: RoleMasterService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
  ) {
    this.isLoadingResults = false;
    this.RoleListing = [];
    this.editstatus = Permission.permission('role', 'edit');
    this.createtatus = Permission.permission('role', 'create');
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchRoleListing();
  }
  /*
  * function use to  fetch RoleListing api
  * @param
  * @memberof RoleMasterComponent
  */
  fetchRoleListing() {
    this.isLoadingResults = true;
    this.RoleApi.rolelisting().subscribe((response: RoleListingApiResponce) => {
      // console.log('fetchRoleListing', response);
      this.isLoadingResults = false;
      if (response.resCode === 200) {
        this.RoleListing = response.data;
      }
      if (this.RoleListing.length <= 0) {
        this.showMessage = 'No Role Avaliable';
      }
    }, (errorData: any) => {
      this.isLoadingResults = false;
      if (this.RoleListing.length <= 0) {
        this.showMessage = 'No Role Avaliable';
      }
    });
  }
  /*
  * function use to  open editRole type modal
  * @param
  * @memberof RoleMasterComponent
  */
  editRoletype(object) {
    const dialogRef = this.dialog.open(EditRoleComponent, {
      panelClass: 'dialog-sm',
      disableClose: true,
      data: object,
      width: '800px'
    });
    dialogRef.componentInstance.OnEditRole.subscribe((data: any) => {
      this.fetchRoleListing();
    });
  }

}

