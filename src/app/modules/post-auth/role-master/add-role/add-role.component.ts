import { AppConst } from '../../../../app.constants';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { ADDRoleApiResponce } from 'src/app/models/role';


@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss'],
  providers: []
})
export class AddRoleComponent implements OnInit {
  addRoleform: FormGroup;
  showLoader: boolean;
  adminusermanagement: Array<any>;
  @Output() OnAddRole = new EventEmitter<ADDRoleApiResponce>(true);
  constructor(
    private formBulder: FormBuilder,
    private toast: ToastProvider,
    private AddRoleApi: RoleMasterService,
    private consoleProvider: ConsoleProvider,
    private dialogRef: MatDialogRef<AddRoleComponent>,

  ) {
    this.adminusermanagement = AppConst.USER_MANAGEMET;
  }
  /*
  * function use to call build role form
  *
  * @memberof AddRoleComponent
  */
  buildRoleFrom() {
    this.addRoleform = new FormGroup({
      roleName: new FormControl('', [
        Validators.required,
      ]),
      facility: this.formBulder.array([])
    });
    for (let i = 0; i < this.adminusermanagement.length; i++) {
      this.facility.push(this.addPackagefacility(this.adminusermanagement[i]));
      // tslint:disable-next-line:prefer-for-of
      for (let j = 0; j < this.adminusermanagement[i].permission.length; j++) {
        this.addRoleform.get('facility').updateValueAndValidity();
        this.permission(i).push(this.initPermisson(this.adminusermanagement[i].permission[j]));
      }
    }
  }
  ngOnInit() {
    this.buildRoleFrom();
  }
  /*
  * function use to dynamicaly add facility
  *
  * @memberof AddRoleComponent
  */
  addPackagefacility(param) {
    return this.formBulder.group({
      name: [param.name, [Validators.required]],
      status: [param.status, [Validators.required]],
      permission: this.formBulder.array([])
    });
  }
  get facility(): FormArray {
    return this.addRoleform.get('facility') as FormArray;
  }
  permission(index): FormArray {
    return this.addRoleform.get('facility')['controls'][index]['controls'].permission as FormArray;
  }
  /*
  * function use to call dynamicaly add permisson
  *
  * @memberof AddRoleComponent
  */
  initPermisson(param) {
    return this.formBulder.group({
      permissionName: [param.permissionName, [Validators.required]],
      status: [param.status, [Validators.required]],
    });
  }
  /*
  * function use to call api for add role
  *
  * @memberof AddRoleComponent
  */
  addRole(roleInfo) {
    if (this.addRoleform.valid) {
      this.showLoader = true;
      this.AddRoleApi.addrole(roleInfo).subscribe((response: ADDRoleApiResponce) => {
        this.showLoader = false;
        if (response.success) {
          this.OnAddRole.emit(response);
          this.dialogRef.close();
          this.toast.success('Role Added Successfully');
        }
      }, (errorData: any) => {
        this.showLoader = false;
      });
    }
  }
}
