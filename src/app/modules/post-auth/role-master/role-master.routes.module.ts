import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../services/guards/auth-guard.service';
import { RoleMasterComponent } from './role-master.component';


const routerConfig: Routes = [
  { path: '', component: RoleMasterComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class RoleMasterRouteModule { }
