
import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { RoleMasterService } from 'src/app/services/apis/role.service';

@Component({
  selector: 'app-delete-role',
  templateUrl: './delete-role.component.html',
  styleUrls: ['./delete-role.component.scss'],
  providers: []
})
export class DeleteRoleComponent implements OnInit {
  public showLoader: boolean;
  roleId: number;
  @Output() OnDeleteRole = new EventEmitter<any>(true);
  constructor(
    public dialogRef: MatDialogRef<DeleteRoleComponent>,
    public toast: ToastProvider,
    private RoleApi: RoleMasterService,
    private consoleProvider: ConsoleProvider,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.showLoader = false;
    // this.roleId = this.data;
  }

  ngOnInit() {
  }

  deleterole() {
    this.showLoader = true;
    // this.RoleApi.deleterole(this.roleId).subscribe((response: any) => {
    //   this.showLoader = false;
    //   // if (response.success) {
    //   //   this.OnDeleteRole.emit(response);
    //   //   this.dialogRef.close();
    //   //   this.toast.success('Role Deleted Successfully');
    //   // }
    // }, (errorData: any) => {
    //   this.showLoader = false;
    // });
  }

}
