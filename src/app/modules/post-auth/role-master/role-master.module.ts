import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { RoleMasterRouteModule } from './role-master.routes.module';
import { RoleMasterComponent } from './role-master.component';
import { AddRoleComponent } from './add-role/add-role.component';
import { DeleteRoleComponent } from './delete-role/delete-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { RoleMasterService } from 'src/app/services/apis/role.service';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
@NgModule({
  imports: [
    RoleMasterRouteModule,
    SharedModule,
  ],
  declarations: [
    RoleMasterComponent,
    EditRoleComponent,
    DeleteRoleComponent,
    AddRoleComponent
  ],
  entryComponents: [
    RoleMasterComponent,
    EditRoleComponent,
    DeleteRoleComponent,
    AddRoleComponent
  ],

  providers: [RoleMasterService ,PermissionProvider]
})
export class RoleMasterModule { }
