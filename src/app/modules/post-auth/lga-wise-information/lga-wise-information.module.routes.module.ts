import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { LGAWiseInformationReportComponent } from './lga-wise-information.component';


const routerConfig: Routes = [
  { path: '', component: LGAWiseInformationReportComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class LGAWiseInformationRouteModule { }
