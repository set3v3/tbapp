

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import * as _ from 'lodash';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { DashBordService } from 'src/app/services/apis/dashbord.service';
import { LgaWiseReportApiResponce } from 'src/app/models/dashbord';

@Component({
  selector: 'app-lgawiseinformation',
  templateUrl: './lga-wise-information.component.html',
  styleUrls: ['./lga-wise-information.component.scss'],
  providers: []
})
export class LGAWiseInformationReportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  LGAWiseInformationlisting: Array<any>;
  totalItem: number;
  userInfo: any;
  page: number;
  pageindex: number;
  usertype: string;
  displayedColumns: Array<any> = ['lga', 'Screened', 'Presumptive', 'Positive', 'Negative', 'Treatment'];
  displayedColumnsuperadmin: Array<any> = ['lga', 'Screened', 'Presumptive', 'Positive', 'Negative', 'Treatment', 'active', 'lapsed'];
  dataSourcesuperadmin: MatTableDataSource<any>;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private consoleProvider: ConsoleProvider,
    public dialog: MatDialog,
    private DashbordApi: DashBordService,
    private Permission: PermissionProvider,
    private router: Router,
    private route: ActivatedRoute,
    private userStorage: UserStorageProvider,
  ) {
    this.userInfo = this.userStorage.get();
    this.isLoadingResults = false;
    this.usertype = this.userInfo.data.role.roleName;
    this.showMessage = '';
    //console.log(this.usertype);
    this.LGAWiseInformationlisting = [];

    // tslint:disable-next-line:triple-equals

  }

  ngOnInit() {
    this.fetchReportListing(0);
  }
  /*
  * function use for pagenation
  * @param
  * @memberof LGAWiseInformationReportComponent
  */
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchReportListing(event.pageIndex, event.pageSize);
  }
  /*
  * function use for fetch ReportListing
  * @param
  * @memberof LGAWiseInformationReportComponent
  */
  fetchReportListing(index?, limit?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    const param = {
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      roleName: this.usertype
    };
    this.DashbordApi.getLGAWiseInformationTable(param).subscribe((res: LgaWiseReportApiResponce) => {
      this.isLoadingResults = false;
      if (res.success) {
        this.LGAWiseInformationlisting = res.data;
        this.totalItem = res.totalCount;
        // tslint:disable-next-line:triple-equals
        if (this.usertype != 'Generalist user') {
          this.dataSourcesuperadmin = new MatTableDataSource(this.LGAWiseInformationlisting);
          this.dataSourcesuperadmin.sort = this.sort;
        } else {
          this.dataSource = new MatTableDataSource(this.LGAWiseInformationlisting);
          this.dataSource.sort = this.sort;
        }
      }
      if (this.LGAWiseInformationlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      // console.log('api_http-------errr', err);
    });
  }
  /*
  * function use for navigate back to dashbord
  * @param
  * @memberof LGAWiseInformationReportComponent
  */
  goDashbord() {
    this.router.navigate(['/post-auth/dashboard']);
  }
}

