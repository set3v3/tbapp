import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSortModule } from '@angular/material';
import { LGAWiseInformationReportComponent } from './lga-wise-information.component';
import { LGAWiseInformationRouteModule } from './lga-wise-information.module.routes.module';
import { DashBordService } from 'src/app/services/apis/dashbord.service';

@NgModule({
  imports: [
    LGAWiseInformationRouteModule,
    SharedModule,
    MatSortModule
  ],
  declarations: [
    LGAWiseInformationReportComponent
  ],
  entryComponents: [LGAWiseInformationReportComponent
  ],
  providers: [DashBordService]
})
export class LGAWiseInformationModule { }
