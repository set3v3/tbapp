import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastProvider } from '../../../../shared/modules/toast/toast.provider';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';




@Component({
  selector: 'app-view-tb-confirmation-report',
  templateUrl: './view-tb-confirmation-report.component.html',
  styleUrls: ['./view-tb-confirmation-report.component.scss'],
  providers: []
})
export class ViewTbConfirmationReportComponent implements OnInit {
  tbConfirmationReportInfo: any;
  showLoader: boolean;

  constructor(
    private toast: ToastProvider,
    private dialogRef: MatDialogRef<ViewTbConfirmationReportComponent>,
    private consoleProvider: ConsoleProvider,
    @Inject(MAT_DIALOG_DATA) public data: any

  ) {
    this.tbConfirmationReportInfo = this.data;
    this.showLoader = false;
    this.consoleProvider.log('ViewTbConfirmationReportComponent', this.tbConfirmationReportInfo);


  }
  ngOnInit() {

  }

}
