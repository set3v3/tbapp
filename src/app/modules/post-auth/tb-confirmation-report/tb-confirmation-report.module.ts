import { NgModule } from '@angular/core';
import { TbConfrimationReportRouteModule } from './tb-confirmation-report.routes.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { ReportService } from 'src/app/services/apis/report.service';
import { TbConfrimationReportComponent } from './tb-confirmation-report.component';
import { ViewTbConfirmationReportComponent } from './view-tb-confirmation-report/view-tb-confirmation-report.component';
import { MatSortModule } from '@angular/material';

@NgModule({
  imports: [
    TbConfrimationReportRouteModule,
    SharedModule,
    MatSortModule

  ],
  declarations: [
    TbConfrimationReportComponent,
    ViewTbConfirmationReportComponent
  ],
  entryComponents: [
    TbConfrimationReportComponent,
    ViewTbConfirmationReportComponent
  ],
  providers: [ReportService]
})
export class TbConfrimationReportModule { }
