import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth-guard.service';
import { TbConfrimationReportComponent } from './tb-confirmation-report.component';
const routerConfig: Routes = [
  { path: '', component: TbConfrimationReportComponent, canActivate: [AuthGuard], pathMatch: 'full' },
];
@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class TbConfrimationReportRouteModule { }
