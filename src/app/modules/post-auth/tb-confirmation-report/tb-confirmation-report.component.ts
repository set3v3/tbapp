

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, PageEvent, MatSort } from '@angular/material';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { UserlistApiResponse } from 'src/app/models/usermaster';
import { ReportService } from 'src/app/services/apis/report.service';
import * as _ from 'lodash';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { ViewTbConfirmationReportComponent } from './view-tb-confirmation-report/view-tb-confirmation-report.component';
import { AppConst } from 'src/app/app.constants';
import { ExportReportApiResponce, TbConfirmedReportReportApiResponce } from 'src/app/models/report';

@Component({
  selector: 'app-tb-confirmation-report',
  templateUrl: './tb-confirmation-report.component.html',
  styleUrls: ['./tb-confirmation-report.component.scss'],
  providers: []
})
export class TbConfrimationReportComponent implements OnInit {
  isLoadingResults: boolean;
  showMessage: string;
  TbConfrimationReportlisting: Array<any>;
  totalItem: number;
  page: number;
  pageindex: number;
  searchParam: any;
  viewstatus: boolean;
  bydefatseletedTbStatus: any;
  TbStatus: any;
  displayedColumns = ['firstName', 'middleName', 'lastName', 'phoneNumber', 'age', 'sex', 'confirmedPositive'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private consoleProvider: ConsoleProvider,
    private ReportApi: ReportService,
    public dialog: MatDialog,
    private Permission: PermissionProvider,
  ) {
    this.isLoadingResults = false;
    this.TbConfrimationReportlisting = [];
    this.TbStatus = AppConst.TbStatus;
    this.bydefatseletedTbStatus = 2;
    this.viewstatus = Permission.permission('TB confirmation report', 'view');
    if (this.viewstatus) {
      this.displayedColumns.push('view');
    }
    this.showMessage = '';
  }

  ngOnInit() {
    this.fetchTbConfrimationReportlistingListing(0);
  }
  ViewTbConfirmationReport(object) {
    const dialogRef = this.dialog.open(ViewTbConfirmationReportComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      data: object,
      width: '500px'
    });
  }
  getNext(event: PageEvent) {
    this.consoleProvider.log('getNext event', event);
    this.page = event.pageSize;
    this.pageindex = event.pageIndex;
    this.fetchTbConfrimationReportlistingListing(event.pageIndex, event.pageSize);
  }

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim(); // Remove whitespace
  //   filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
  //   this.ScreenedReportlisting = [];
  //   this.fetchUserListing(0, '', filterValue);
  // }

  fetchTbConfrimationReportlistingListing(index?, limit?, searchTxt?) {
    this.showMessage = '';
    this.isLoadingResults = true;
    this.searchParam = {
      // userType: (searchTxt) ? searchTxt : '',
      SerachTag: this.bydefatseletedTbStatus,
      page: (limit) ? limit : 10,
      pageIndex: (index) ? (index) : 0,
      report_type: 'confirmedPositive'
    };
    this.ReportApi.getTbconfromationReportList(this.searchParam).subscribe((res: TbConfirmedReportReportApiResponce) => {
      this.isLoadingResults = false;
      // this.consoleProvider.log('getUserList', res);
      if (res.success) {
        this.TbConfrimationReportlisting = res.data;
        this.totalItem = res.totalCount;
        this.dataSource = new MatTableDataSource(this.TbConfrimationReportlisting);
        this.dataSource.sort = this.sort;
        // this.dataSource.paginator = this.paginator;
      }
      if (this.TbConfrimationReportlisting.length <= 0) {
        this.showMessage = 'No Record Found.';
      }
    }, (err) => {
      this.isLoadingResults = false;
      console.log('api_http-------errr', err);
    });
  }
  downloadExport() {
    this.isLoadingResults = true;
    const param = { report_type: 'confirmedPositive' };
    this.ReportApi.downloadReportExport(param).subscribe((res: ExportReportApiResponce) => {
      this.isLoadingResults = false;
      if (res.success) {
        window.location.href = res.data;
      }
    }, (err) => {
      this.isLoadingResults = false;
    });
  }
  TbStatusChange(param) {
    // console.log(this.paginator);
    if (typeof this.paginator !== 'undefined') {

      this.paginator.pageIndex = 0;
    }
    this.bydefatseletedTbStatus = param.value;
    this.fetchTbConfrimationReportlistingListing(0);
  }
}

