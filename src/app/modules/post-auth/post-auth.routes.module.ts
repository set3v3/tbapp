import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostAuthComponent } from './post-auth.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../../services/guards/auth-guard.service';
import { Error404Component } from '../../components/error-404/error-404.component';
import { NoAccessComponent } from '../../components/no-access/no-access-component';

const routerConfig: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: '',
    component: PostAuthComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], pathMatch: 'full' },
      { path: 'user', loadChildren: './user-master/user-master.module#UserMasterModule' },
      { path: 'user-role', loadChildren: './role-master/role-master.module#RoleMasterModule' },
      { path: 'admin-user', loadChildren: './user-admin/user-admin.module#UserAdminModule' },
      { path: 'facility', loadChildren: './facility-master/facility-master.module#FacilityMasterModule' },
      { path: 'referred-report', loadChildren: './reffered-report/reffered-report.module#RefferedReportModule' },
      { path: 'tb-confirmation-report', loadChildren: './tb-confirmation-report/tb-confirmation-report.module#TbConfrimationReportModule' },
      { path: 'treatment-report', loadChildren: './treatment-report/treatment-report.module#TreatmentReportModule' },
      { path: 'screened-report', loadChildren: './screened-report/screened-report.module#ScreenedReportModule' },
      // tslint:disable-next-line:max-line-length
      { path: 'referral-incentive-report', loadChildren: './referral-incentive-report/referral-incentive-report.module#ReferralIncentiveReportModule' },
      // tslint:disable-next-line:max-line-length
      { path: 'presumptive-not-referred-report', loadChildren: './presumptive-not-referred-report/presumptive-not-referred-report.module#PresumptiveNotReferredReportModule' },
      { path: 'lga-wise-information', loadChildren: './lga-wise-information/lga-wise-information.module#LGAWiseInformationModule' },
      // tslint:disable-next-line:max-line-length
      { path: 'referral-patients-details', loadChildren: './referral-patients-details/referral-patients-details.module#ReferralPatientsDetailsModule' },
      { path: 'help-support', loadChildren: './help-support/help-support.module#HelpSupportModule' },
      { path: '404', component: Error404Component },
      { path: 'no-access', component: NoAccessComponent }
    ],
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: '404', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  declarations: [],
  providers: [AuthGuard]
})
export class PostAuthRouteModule { }
