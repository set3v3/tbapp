import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PreAuthComponent } from './pre-auth.component';
import { SessionGuard } from '../../services/guards/session-guard.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { TramConditionComponent } from './tram-and-condition/tram-and-condition';


const routerConfig: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path: 'login', redirectTo: 'login/store-manager', pathMatch: 'full' },
  {
    path: '',
    component: PreAuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
      { path: 'license-agreement', component: TramConditionComponent }
    ],
    canActivate: [SessionGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routerConfig)
  ],
  exports: [RouterModule],
  providers: [SessionGuard]
})
export class PreAuthRouteModule {
}
