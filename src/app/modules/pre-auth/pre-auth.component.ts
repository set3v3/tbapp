import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pre-auth',
  template: `<router-outlet></router-outlet>`
})
export class PreAuthComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
