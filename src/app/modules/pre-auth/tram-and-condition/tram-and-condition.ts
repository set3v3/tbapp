import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//  * @export
//  * @class TramConditionComponent
//  * @implements {OnInit}
//  * @author tathagata sur
//  */
@Component({
  selector: 'app-tramandcondition',
  templateUrl: './tram-and-condition.html',
  styleUrls: ['./tram-and-condition.scss']
})
export class TramConditionComponent implements OnInit {

  iaccpect: boolean;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.iaccpect = false;
    /** set default value of properties */
  }

  ngOnInit() {
  }

  gotoLoginpage() {
    this.router.navigate(['/pre-auth/login']);
  }
  accept(event) {
    this.iaccpect = event.checked;
  }

}
