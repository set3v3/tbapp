import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/apis/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastProvider } from '../../../shared/modules/toast/toast.provider';
import { UserStorageProvider } from '../../../services/storage/user-storage.service';
import { LoginApiResponce } from 'src/app/models/auth';
import { ConsoleProvider } from 'src/app/shared/modules/console/console.provider';
import { AppConst } from 'src/app/app.constants';
// /**
//  * LoginComponent is designed to manages the Login related stuffs.
//  *
//  * @export
//  * @class LoginComponent
//  * @implements {OnInit}
//  * @author TATHAGATA SUR
//  */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public loading: boolean;
  public referer: string;
  public usertype: Array<any>;
  constructor(
    private fb: FormBuilder,
    private authProvider: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastProvider,
    private consoleProvider: ConsoleProvider,
    private userStorage: UserStorageProvider
  ) {
    /** set default value of properties */
    this.loading = false;
    /** invoke Building Login form */
    this.buildLoginForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params.hasOwnProperty('referer')) {
        this.referer = params.referer;
      }
    });
  }
  /*
     * function use to build Login Form
     *
     * @memberof LoginComponent
     */
  buildLoginForm() {
    // tslint:disable */
    // tslint:disable-next-line:max-line-length
    const emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // tslint:enable */

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(emailRegx)]],
      password: ['', Validators.required],
      // lastLoginTime: [''],
      // lastLogoutTime: [''],
      // ip: ['118.185.74.150'],
      // browser: ['chrome']
    });
  }
  /*
     * function use for  dologin
     * @ param userName ,userType,password
     * @memberof LoginComponent
     */
  onSubmit() {
    this.loading = true;
    this.authProvider.dologin(this.loginForm.value).subscribe((responseData: any) => {
      this.loading = false;
      this.consoleProvider.log('dologin', responseData);
      if (responseData.success) {
        this.userStorage.clear();
        this.userStorage.set(responseData);
        this.referer ? this.router.navigate([this.referer]) : this.router.navigate(['post-auth']);
      }
    }, error => {
      this.loading = false;
    });
  }
}
