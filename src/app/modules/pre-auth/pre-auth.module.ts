import { NgModule } from '@angular/core';
import { PreAuthComponent } from './pre-auth.component';
import { LoginComponent } from './login/login.component';
import { PreAuthRouteModule } from './pre-auth.routes.module';
import { SharedModule } from '../../shared/shared.module';
import { AuthService } from '../../services/apis/auth.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PermissionProvider } from 'src/app/services/permission/permission.service';
import { TramConditionComponent } from './tram-and-condition/tram-and-condition';

@NgModule({
  imports: [
    PreAuthRouteModule,
    SharedModule
  ],
  declarations: [PreAuthComponent, LoginComponent, TramConditionComponent, ForgotPasswordComponent],
  exports: [],
  providers: [AuthService]
})
export class PreAuthModule { }
