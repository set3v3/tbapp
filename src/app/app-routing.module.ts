import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './components/error-404/error-404.component';
import { ComponentsModule } from './components/components.module';
const routerConfig: Routes = [
  { path: 'pre-auth', loadChildren: './modules/pre-auth/pre-auth.module#PreAuthModule' },
  { path: 'post-auth', loadChildren: './modules/post-auth/post-auth.module#PostAuthModule' },
  { path: '404', component: Error404Component },
  { path: '', redirectTo: 'pre-auth', pathMatch: 'full' },
  { path: '**', redirectTo: '404', pathMatch: 'full' }
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routerConfig, { enableTracing: true }),
    RouterModule.forRoot(routerConfig, {}),
    ComponentsModule
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
