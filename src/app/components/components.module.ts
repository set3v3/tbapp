import { NgModule } from '@angular/core';
import { Error404Component } from './error-404/error-404.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SharedModule } from '../shared/shared.module';
import { NoAccessComponent } from './no-access/no-access-component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    SharedModule,
    FormsModule
  ],
  declarations: [Error404Component, HeaderComponent, FooterComponent, NoAccessComponent],
  exports: [Error404Component, HeaderComponent, FooterComponent]
})
export class ComponentsModule { }
