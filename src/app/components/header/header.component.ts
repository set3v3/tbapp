import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserStorageProvider } from 'src/app/services/storage/user-storage.service';
import { MatDialog } from '@angular/material';
import { ChangepasswordComponent } from 'src/app/modules/post-auth/change-password/change-password.component';
import { AuthService } from 'src/app/services/apis/auth.service';
import { ToastProvider } from 'src/app/shared/modules/toast/toast.provider';
import { LogOutApiResponce } from 'src/app/models/auth';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [AuthService, ToastProvider]
})
export class HeaderComponent implements OnInit {

  @Output() navToggle = new EventEmitter<boolean>();
  userInfo: any;
  constructor(
    private router: Router,
    private userStorage: UserStorageProvider,
    private dialog: MatDialog,
    private authApi: AuthService,
    private toast: ToastProvider,
  ) {
    this.userInfo = this.userStorage.get();
  }

  ngOnInit() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width > 1024) {
      this.navToggle.emit(true);
    }
  }

  navBtnClicked() {
    this.navToggle.emit(true);
  }
  logOut() {
    this.authApi.logout().subscribe((responseData: LogOutApiResponce) => {
      if (responseData.success) {
        this.router.navigate(['/']);
        this.userStorage.clear();
        this.toast.success(responseData.message);
      }
    }, error => {
    });
    // this.router.navigate(['/']);
    // this.userStorage.clear();
  }
  /*
  * function use to open chnage password modal
  *
  * @memberof PostAuthComponent
  */
  changePassword() {
    const dialogRef = this.dialog.open(ChangepasswordComponent, {
      panelClass: 'dialog-xs',
      disableClose: true,
      // data: this.catagoriesListing[catagoryIndex]
    });
  }
}
